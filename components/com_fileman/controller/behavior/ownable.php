<?php
/**
 * @package     FILEman
 * @copyright   Copyright (C) 2011 - 2014 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

class ComFilemanControllerBehaviorOwnable extends KControllerBehaviorAbstract
{
    /**
     * The current active menu item.
     *
     * @var object|null
     */
    protected $_menu;

    /**
     * The user folder.
     *
     * @var string|null
     */
    protected $_folder;

    /**
     * The file container.
     *
     * @var string
     */
    protected $_container;

    /**
     * Folders model.
     *
     * @var mixed
     */
    protected $_model;

    public function __construct(KObjectConfig $config)
    {
        parent::__construct($config);

        $this->_container = $config->container;
        $this->_model     = $config->model;
    }

    protected function _initialize(KObjectConfig $config)
    {
        $config->append(array('container' => 'fileman-files', 'model' => 'com:files.model.folders'));
        parent::_initialize($config);
    }

    public function isSupported()
    {
        $result = false;

        $user = $this->getMixer()->getUser();
        $menu = $this->_getActiveMenu();

        if ($user->isAuthentic() && $menu && $menu->query['view'] == 'userfolder') {
            $result = true;
        }

        return $result;
    }

    public function onMixin(KObjectMixable $mixer)
    {
        parent::onMixin($mixer);

        if ($this->isSupported())
        {
            $query = $mixer->getRequest()->getQuery();

            // Force user folder iff username is not already present on it, i.e. accessing menu item's root.
            if ($this->_getBasePath() == $query->folder) {
                $query->folder = $this->getFolder();
            }
        }
    }

    protected function _beforeRender(KControllerContextInterface $context)
    {
        // Checks that the current folder can be accessed.
        if (!$this->canAccess($context->getRequest()->getQuery()->folder))
        {
            $message = ucfirst($context->getAction()) . ' not allowed';
            throw new KControllerExceptionRequestForbidden($message);
        }
    }

    protected function _beforeAdd(KControllerContextInterface $context)
    {
        // Same as render
        $this->_beforeRender($context);
    }

    /**
     * Access permission check.
     *
     * @param string $folder The folder to check.
     * @return bool True if folder can be accessed, false otherwise.
     */
    public function canAccess($folder)
    {
        $result = true;

        $folder = (string) $folder;

        if (strpos($folder, $this->getFolder()) !== 0) {
            $result = false;
        }

        return $result;
    }

    /**
     * User folder getter.
     *
     * @throws RuntimeException if a user folder cannot be created.
     *
     * @return string|null The user folder, null if the there is no user folder, i.e. the user is not authenticated.
     */
    public function getFolder()
    {
        if (!$this->_folder)
        {
            $user = $this->getMixer()->getUser();

            if ($user->isAuthentic())
            {
                $base_path = $this->_getBasePath();

                $filter = $this->getObject('com://site/fileman.filter.userfolder');

                $folder = $base_path . '/' . $filter->sanitize($user->getUsername());

                if (!$this->_folderExists($folder) && !$this->_addFolder($folder))
                {
                    $folder = $base_path . '/id-' . $user->getId();

                    if (!$this->_folderExists($folder) && !$this->_addFolder($folder)) {
                        throw new RuntimeException('Unable to create user folder '. $folder);
                    }
                }
            }
            else $folder = null;

            $this->_folder = $folder;
        }

        return $this->_folder;
    }

    /**
     * Active menu item getter.
     *
     * @throws RuntimeException if there is no active menu item.
     *
     * @return object The active menu item
     */
    protected function _getActiveMenu()
    {
        if (!$this->_menu) {
            $this->_menu = JFactory::getApplication()->getMenu()->getActive();
        }

        return $this->_menu;
    }

    /**
     * User folder base path getter.
     *
     * @return string The folder base path.
     */
    protected function _getBasePath()
    {
        $menu      = $this->_getActiveMenu();
        $base_path = '';

        if (isset($menu->query['folder'])) {
            $base_path = $menu->query['folder'];
        }

        return trim($base_path, '/');
    }

    /**
     * Checks if a given folder in a given container already exists.
     *
     * @param string $folder The folder to check.

     * @return bool True if the folder exists, false otherwise.
     */
    protected function _folderExists($folder)
    {
        $parts = explode('/', $folder);
        $name = array_pop($parts);

        $state = array(
            'container' => $this->_container,
            'folder'    => implode('/', $parts),
            'name'      => $name
        );

        $folder = $this->_getModel()->setState($state)->fetch();

        return (bool) count($folder);
    }

    /**
     * Creates a new folder in a given container.
     *
     * @param string $folder The folder to create.

     * @return bool True if the folder was successfully created, false otherwise.
     */
    protected function _addFolder($folder)
    {
        $parts = explode('/', $folder);
        $name = array_pop($parts);

        $state = array(
            'container' => $this->_container,
            'folder'    => implode('/', $parts),
            'name'      => $name
        );

        $result = $this->_getModel()->create($state)->save();

        return $result === false ? false : true;
    }

    protected function _getModel()
    {
        if (!$this->_model instanceof KModelInterface) {
            $this->_model = $this->getObject($this->_model);
        }

        $this->_model->reset();

        return $this->_model;
    }
}