<?php
/**
 * @package     FILEman
 * @copyright   Copyright (C) 2011 - 2014 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

class ComFilemanControllerUserfolder extends ComFilemanControllerFolder
{
    public function __construct(KObjectConfig $config)
    {
        parent::__construct($config);

        $this->addCommandCallback('before.render', '_setInternals');
        $this->addCommandCallback('before.render', '_checkUser');
    }

    protected function _initialize(KObjectConfig $config)
    {
        $config->append(array('behaviors' => array('ownable'), 'model' => 'com:files.model.folders'));
        parent::_initialize($config);
    }

    protected function _actionRender(KControllerContextInterface $context)
    {
        $result = false;

        //Execute the action
        if($this->execute('browse', $context) !== false) {
            $result = parent::_actionRender($context);
        }

        return $result;
    }

    protected function _checkUser(KControllerContextInterface $context)
    {
        if (!$context->user->isAuthentic())
        {
            $message  = $this->getObject('translator')->translate('You need to be logged in to access your files');
            $url      = JRoute::_('index.php?Itemid=' . $this->getRequest()->getQuery()->Itemid, false);
            $redirect = JRoute::_('index.php?option=com_users&view=login&return='.base64_encode($url), false);

            JFactory::getApplication()->redirect($redirect, $message, 'error');
        }
    }

    protected function _setInternals(KControllerContextInterface $context)
    {
        $internals = array('sort', 'direction', 'container');

        $state = $this->getModel()->getState();

        foreach ($internals as $internal) {
            $state->setProperty($internal, 'internal', true);
        }
    }

    public function getView()
    {
        $view = parent::getView();

        if ($this->isOwnable()) {
            $view->setUserFolder($this->getFolder());
        }

        return $view;
    }
}