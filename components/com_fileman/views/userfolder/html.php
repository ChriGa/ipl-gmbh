<?php
/**
 * @package     FILEman
 * @copyright   Copyright (C) 2011 - 2014 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

class ComFilemanViewUserfolderHtml extends ComKoowaViewHtml
{
    protected $_user_folder;

    protected function _initialize(KObjectConfig $config)
    {
        $config->auto_fetch = false;
        parent::_initialize($config);
    }

    public function setUserFolder($folder)
    {
        $this->_user_folder = (string) $folder;
        return $this;
    }

    public function getUserFolder()
    {
        return $this->_user_folder;
    }

    protected function _fetchData(KViewContext $context)
	{
        parent::_fetchData($context);

        $menu   = JFactory::getApplication()->getMenu()->getActive();
        $params = new ComKoowaDecoratorParameter(new KObjectConfig(array('delegate' => $menu->params)));

        $model = $this->getModel();

        $folders = $model->fetch();

        $humanize = $params->humanize_filenames;

        foreach ($folders as $folder) {
            if ($humanize) {
                $folder->display_name = ucfirst(preg_replace('#[-_\s\.]+#i', ' ', $folder->name));
            }
            else $folder->display_name = $folder->name;
        }

        $state = $this->getModel()->getState();


        $query = $state->getValues();

		$query['thumbnails'] =  (bool) $params->show_thumbnails;

		if ($this->getLayout() === 'gallery') {
			$query['types'] = array('image');
		}

		$controller = $this->getObject('com:files.controller.file');
        $controller->getRequest()->setQuery($query);

		$files = $controller->browse();
        $total = $controller->getModel()->count();

		foreach ($files as $file)
        {
            if ($humanize) {
                $file->display_name = ucfirst(preg_replace('#[-_\s\.]+#i', ' ', $file->filename));
            }
            else $file->display_name = $file->name;
		}

        if (!$params->page_heading) {
            $params->page_heading = $menu->title;
        }

        $parts = explode('/', $state->folder);

        $name = array_pop($parts);

        $current = $this->getObject('com:files.model.folders')
                        ->container('fileman-files')
                        ->folder(implode('/', $parts))
                        ->name($name)
                        ->fetch();

        if ($this->getUserFolder() != $current->path) {
            $path = trim(str_replace($this->getUserFolder(), '', $current->path), '/');
        }
        else $path = null;

        // As expected by upload.html.php
        $context->data->folder         = (object) array('path' => $context->parameters->folder);
        $context->data->current        = $current;
        $context->data->path           = $path;
        $context->data->folders        = $folders;
        $context->data->files          = $files;
        $context->data->folders        = $folders;
        $context->data->total          = $total;
        $context->data->params         = $params;
        $context->data->thumbnail_size = array('x' => 200, 'y' => 150);

        $this->_setPathway($context);

        $context->parameters->total = $total;
	}

    protected function _setPathway(KViewContext $context)
    {
        if ($context->data->path !== null)
        {
            $pathway = JFactory::getApplication()->getPathway();

            $path   = explode('/', $context->data->path);
            $folder = $this->getUserFolder();

            foreach ($path as $part)
            {
                $folder .= '/' . $part;
                $link = $this->getRoute('layout=' . $this->getLayout() . '&view=user&folder=' . $folder);

                $pathway->addItem(ucfirst($part), $link);
            }
        }
    }
}