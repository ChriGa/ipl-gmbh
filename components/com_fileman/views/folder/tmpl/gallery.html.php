<?
/**
 * @package     FILEman
 * @copyright   Copyright (C) 2012 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */
defined('_JEXEC') or die; ?>

<?= helper('bootstrap.load', array('javascript' => true)); ?>
<?= helper('behavior.jquery'); ?>
<?= helper('behavior.modal'); ?>

<?= import('com://site/fileman.folder.photoswipe.html'); ?>

<ktml:script src="media://com_fileman/js/site/gallery.js" />

<script>
    kQuery(function($) {
        var categoriesGallery = $('.koowa_media_wrapper--categories');
        if ( categoriesGallery ) {
            categoriesGallery.simpleGallery();
        }

        var documentsGallery = $('.koowa_media_wrapper--documents');
        if ( documentsGallery ) {
            documentsGallery.simpleGallery();
        }

        <? if ($params->track_views): ?>
        $('.fileman-view').click(function() {
            Fileman.trackEvent({action: 'Download', label: $(this).attr('data-path')});
        });

        $(document).on('photoswipeImageView', function(event, item) {
            if (item.path) {
                Fileman.trackEvent({action: 'Download', label: item.path});
            }
        });
        <? endif; ?>
    });
</script>

<?
// @TODO: we might want to add this to the bootstrap file so it's everywhere
// Advantages of this technique:
// 1. We can make sure there's no flashing content before JS is initialized
//    (by adding html.js-enabled .container {} html.js-enabled .container.js-initialized etc. {} )
//    but keep good functionality when JS is disabled/broken
// 2. We're not using a JS framework but this is super fast, also no extra http request
?>
<script>function addClass(e, c) {if (e.classList) {e.classList.add(c);} else {e.className += ' ' + c;}}var el = document.documentElement;var cl = 'koowa-js-enabled';addClass(el,cl)</script>


<div itemprop="mainContentOfPage" itemscope itemtype="http://schema.org/ImageGallery">

    <? if ($params->show_page_heading): ?>
    	<h2 class="fileman_header" itemprop="name">
    		<?= escape($params->page_heading) ?>
    	</h2>
    <? endif ?>

    <? if ($params->allow_uploads): ?>
        <?= import('upload.html', array('folder' => $folder)); ?>
    <? endif; ?>

    <? if (count($files) || count($folders)): ?>
    <? // Documents & pagination  ?>
    <form action="" method="get" class="-koowa-grid">
        <div class="koowa_media--gallery">
            <div class="koowa_media_wrapper koowa_media_wrapper--categories">
                <div class="koowa_media_contents">
                    <?php // this comment below must stay ?>
                    <div class="koowa_media"><!--
                        <? foreach($folders as $folder): ?>
                 -->    <div class="koowa_media__item">
                            <div class="koowa_media__item__content">
                                <div class="koowa_header">
                                    <div class="koowa_header__item koowa_header__item--image_container">
                                        <a class="koowa_header__link" href="<?= route('layout=gallery&folder='.$folder->path) ?>">
                                            <span class="koowa_icon--folder koowa_icon--24"><i>folder</i></span>
                                        </a>
                                    </div>
                                    <div class="koowa_header__item">
                                        <div class="koowa_wrapped_content">
                                            <div class="whitespace_preserver">
                                                <div class="overflow_container">
                                                    <a class="koowa_header__link" href="<?= route('layout=gallery&folder='.$folder->path) ?>">
                                                        <?= escape($folder->display_name) ?>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!--
                        <? endforeach ?>
                --></div>
                </div>
            </div>
            <div class="koowa_media_wrapper koowa_media_wrapper--documents">
                <div class="koowa_media_contents">
                    <?php // this comment below must stay ?>
                    <div class="koowa_media"><!--
                    <? foreach ($files as $file): ?>
                 --><div class="koowa_media__item" itemscope itemtype="http://schema.org/ImageObject">
                            <div class="koowa_media__item__content">
                                <?= import('com://site/fileman.file.gallery.html', array(
                                    'file' => $file,
                                    'params' => $params
                                )) ?>
                            </div>
                        </div><!--
                    <? endforeach ?>
             --></div>
                </div>
            </div>
        </div>

        <? // Pagination ?>
        <? if ($params->limit != 0): ?>
            <form action="" method="get" class="-koowa-form">
                <?= helper('paginator.pagination', array(
                    'total'  => parameters()->total,
                    'limit'  => parameters()->limit,
                    'offset' => parameters()->offset,
                    'attribs'    => array(
                        'onchange' => 'this.form.submit();'
                    )
                )) ?>
            </form>
        <? endif ?>

    </form>

    <? endif; ?>
</div>