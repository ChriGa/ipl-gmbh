<?
/**
 * @package     DOCman
 * @copyright   Copyright (C) 2011 - 2014 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */
defined('KOOWA') or die; ?>

<?= helper('bootstrap.load'); ?>

<meta itemprop="contentUrl" content="<?= route('view=file&folder='.parameters()->folder.'&name='.$file->name) ?>">

<? if ($params->show_thumbnails && !empty($file->thumbnail)): ?>
    <? if ($file->width): ?>
    <meta itemprop="width" content="<?= $file->width; ?>">
    <? endif; ?>
    <? if ($file->height): ?>
    <meta itemprop="height" content="<?= $file->height; ?>">
    <? endif; ?>

<a class="koowa_media__item__link"
   data-path="<?= escape($file->path); ?>"
   data-width="<?= $file->width; ?>"
   data-height="<?= $file->height; ?>"
   href="<?= route('view=file&folder='.parameters()->folder.'&name='.$file->name) ?>"
   title="<?= escape($file->display_name) ?>">
<? else: ?>
<a class="fileman-view <?= $file->extension === 'pdf' ? 'koowa_media__item__link koowa_media__item__link--html' : ''; ?>"
    data-title="<?= escape($file->display_name); ?>"
    data-id="<?= 0; ?>"
    href="<?= route('view=file&folder='.parameters()->folder.'&name='.$file->name) ?>"
    title="<?= escape($file->display_name); ?>">
<? endif; ?>

    <? if( $file->thumbnail ): ?>
    <div class="koowa_media__item__thumbnail">
        <img itemprop="thumbnail" src="<?= $file->thumbnail ?>" alt="<?= escape($file->display_name) ?>">
    </div>
    <? else: ?>
    <div class="koowa_media__item__icon">
        <span class="koowa_icon--image koowa_icon--48"></span>
    </div>
    <? endif; ?>

    <? if ($params->show_filenames): ?>
    <div class="koowa_header koowa_media__item__label">
        <div class="koowa_header__item koowa_header__item--title_container">
            <div class="koowa_wrapped_content">
                <div class="whitespace_preserver">
                    <div class="overflow_container">
                        <?= escape($file->display_name) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <? endif; ?>
</a>