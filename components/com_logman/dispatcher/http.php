<?php
/**
 * @package     LOGman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

/**
 * Http Dispatcher
 *
 * @author  Arunas Mazeika <https://github.com/amazeika>
 * @package Joomlatools\Component\LOGman
 */
class ComLogmanDispatcherHttp extends ComKoowaDispatcherHttp
{
    protected function _initialize(KObjectConfig $config)
    {
        $config->append(array('controller' => 'activity', 'behaviors' => array('permissible')));

        parent::_initialize($config);
    }

    /**
     * Loads the admin translations instead of site
     *
     * @param KControllerContextInterface $context
     */
    protected function _loadTranslations(KControllerContextInterface $context)
    {
        $this->getObject('translator')->load('com://admin/logman');
    }

    public function getRequest()
    {
        if (!$this->_request instanceof KDispatcherRequestInterface)
        {
            $request = parent::getRequest();

            $query = $request->getQuery();

            $query->direction = 'DESC';

            $user = $this->getObject('user');

            // Only show frontend activities for frontend users.
            if (!$user->authorise('core.admin.login')) {
                $query->application = 'site';
            }

            $menu = JFactory::getApplication()->getMenu()->getActive();

            if ($menu)
            {
                $params = $menu->params;

                // Force limit.
                $query->limit = $params->get('limit');


                if ($params->get('show_own_activities'))
                {
                    if ($user->isAuthentic()) {
                        $query->user = $user->getId();
                    }
                    else $query->user = -1; // Use invalid user for showing an empty list.
                }

                if ($package = $params->get('show_from_package')) {
                    $query->package = $package;
                }
            }
        }

        return $this->_request;
    }

    public function getController()
    {
        if (!($this->_controller instanceof KControllerInterface))
        {
            $controller = parent::getController();

            $internals = array('application', 'sort', 'limit', 'offset');

            $model = $controller->getModel();
            $state = $model->getState();

            foreach ($internals as $internal) {
                $state->setProperty($internal, 'internal', true);
            }

            $request = $this->getRequest();

            if ($request->isAjax()) {
                $model->fixOffset(false);
                $state->setValues($request->getQuery()->toArray());
            }
        }

        return $this->_controller;
    }
}