<?php
/**
 * @package     DOCman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

/**
 * Http Dispatcher Permission
 *
 * @author  Arunas Mazeika <https://github.com/amazeika>
 * @package Logman\Library\Dispatcher
 */
class ComLogmanDispatcherPermissionHttp extends ComKoowaDispatcherPermissionAbstract
{
    /**
     * Checks if there is an active menu item
     *
     * @throws RuntimeException
     * @return bool
     */
    public function canDispatch()
    {
        $menu = JFactory::getApplication()->getMenu()->getActive();

        // There must be an active menu.
        if (!$menu || $menu->query['option'] !== 'com_logman') {
            throw new RuntimeException($this->getObject('translator')->translate('Invalid menu item'));
        }

        // Only registered users can access LOGman's frontend.
        /*if (!$this->getObject('user')->isAuthentic()) {
            throw new KHttpExceptionUnauthorized();
        }*/

        return true;
    }
}