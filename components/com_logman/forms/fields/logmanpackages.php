<?php
/**
 * @package     LOGman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

JFormHelper::loadFieldClass('groupedlist');

class JFormFieldLogmanpackages extends JFormField
{
    protected $type = 'Logmanpackages';

    protected function getInput()
    {
        if (!class_exists('Koowa')) {
            return '';
        }

        $value = $this->value;
        $el_name = $this->name;

        $multiple = (string) $this->element['multiple'] == 'true';
        $deselect =  (string) $this->element['deselect'] === 'true';
        $id =  isset($this->element['id']) ? (string) $this->element['element_id'] : 'logman_packages_select2';

        $view = KObjectManager::getInstance()->getObject('com://site/logman.view.default');
        $template = $view->getTemplate()
                         ->addFilter('style')
                         ->addFilter('script');

        $attribs = array('class' => 'select2-listbox', 'id' => $id, 'multiple' => $multiple);


        $string = "
        <?= helper('bootstrap.load'); ?>
        <?= helper('listbox.packages', array(
            'prompt' => translate('All components'),
			'select2' => true,
            'name' => \$el_name,
            'deselect' => \$deselect,
            'selected' => \$value,
            'attribs'  => \$attribs
        )); ?>";

        if(version_compare(JVERSION, '3.0', 'ge'))
        {
            $string .= "
            <script>
                kQuery(function($){
                    $('#s2id_<?= \$id ?>').show();
                    $('#<?= \$id ?>_chzn').remove();
                });
            </script>
            ";
        }

        $result = $template->loadString($string, 'php')
                           ->render(array(
                               'el_name'      => $el_name,
                               'value'        => $value,
                               'deselect'     => $deselect,
                               'attribs'      => $attribs,
                               'id'           => $id
                           ));

        return $result;
    }
}
