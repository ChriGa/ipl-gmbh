<?php
/**
 * @package     LOGman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

return array(
    'aliases' => array(
        'com://site/logman.model.activities'         => 'com://admin/logman.model.activities',
        'com://site/logman.template.helper.activity' => 'com://admin/logman.template.helper.activity',
        'mod://site/logman.template.helper.activity' => 'com://admin/logman.template.helper.activity',
        'com://site/logman.template.helper.behavior' => 'com://admin/logman.template.helper.behavior',
        'com://site/logman.view.activities.json'     => 'com://admin/logman.view.activities.json',
        'com://site/logman.view.activities.rss'      => 'com://admin/logman.view.activities.rss'
    )
);