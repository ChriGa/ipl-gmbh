<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>
<?php if ($this->countModules('bottom')) : ?>
	<div class="clear-bottom">
		<div class="row-fluid">
			<jdoc:include type="modules" name="bottom" style="xhtml" />
		</div> 	
	</div>  	
<?php endif; ?>
		
