<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;
?>
<?php if(!$detect->isMobile() || $detect->isTablet()) : ?>
	<header class="header">
		<div class="header-wrapper">
	      <!-- Wrap the .navbar in .container to center it within the absolutely positioned parent. -->
	      <div class="container ">
			<div class="row-fluid">

	           
				<div class="header-inner clearfix">					
					<div id="topMenue" class=" menue-top <?php print (!$detect->isMobile() ) ? ' span8 header-right pull-right' : 'span12'; ?>">
						<jdoc:include type="modules" name="menue-top" style="custom" />
					</div>
				</div>   
			</div>
	      </div> <!-- /.container -->
	    </div>
	</header>
	<?php if($detect->isMobile() ) : ?>
		<script type="text/javascript">
			jQuery(document).ready(function() {
				jQuery('#mobileToggle').on("click", function() {
					jQuery('.dropdown-menu').toggle();
						event.preventDefault();
				});
			});
		</script>
	<?php endif; ?>
<?php endif; ?>