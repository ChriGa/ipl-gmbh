<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$span = 8;
if ($this->countModules('left') && $this->countModules('right')) $span = 6;
if (!$this->countModules('left') && !$this->countModules('right')) $span = 12;

$app = JFactory::getApplication();
$params = $app->getParams();

$isFrontpage = false; //auf Startseite doppelte h1 vermeiden
if ($menu->getActive() == $menu->getDefault()) $isFrontpage = true;

?>

<div class="clear-container">
	<div class="content">
		<div class="row-fluid">
		
			<?php if ($this->countModules('left')) : ?>
				<div class="span3 clear-left IPLsidebar">
					<jdoc:include type="modules" name="left" style="xhtml" />
				</div>				
			<?php endif; ?>
				<?php if ($params->get('show_page_heading') == 1 && $params->get('page_heading') != $params->get('page_title')  ) : ?>
					<div class="page-header">
						<h1> <?php echo $params->get('page_heading'); ?> </h1>
					</div>
				<?php  elseif( !$isFrontpage ) : ?> <?php // Startseite?  ?>
					<div class="page-header">
						<h1> <?php echo $params->get('page_title'); ?> </h1>
					</div>
				<?php endif; ?>

			<main id="content" role="main" class="span<?php echo $span; ?>">
			<!-- Begin Content -->
			<jdoc:include type="modules" name="content-top" style="xhtml" />
			<jdoc:include type="message" />
			<jdoc:include type="component" />
			<jdoc:include type="modules" name="content-center" style="none" />
			<!-- End Content -->
			</main>
			
			<?php if ($this->countModules('right')) : ?>
				<div class="span4 clear-right IPLsidebar">		
					<jdoc:include type="modules" name="right" style="xhtml" />
				</div>
			<?php endif; ?>
			
		</div>
	</div>
</div>