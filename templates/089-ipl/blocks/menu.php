<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;

?>
    <?php if ( !$detect->isMobile() ) : //CG DESKTOP ?>             
		<div class="clear-phone-logo span4">
			<a class="logo animsition-link" href="<?php echo $this->baseurl; ?>">
				<?php echo $logo; ?>
				<?php if ($this->params->get('sitedescription')) : ?>
					<?php echo '<div class="site-description">' . htmlspecialchars($this->params->get('sitedescription')) . '</div>'; ?>
				<?php endif; ?>
			</a>
		</div>
		<nav class="navbar-wrapper span8">
	        <div class="navbar">
	          <div class="navbar-inner">
	           <button type="button" class="btn btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
	              <span class="icon-bar"></span>
	              <span class="icon-bar"></span>
	              <span class="icon-bar"></span>
	            </button>	                     	
				  <?php if ($this->countModules('menu')) : ?>
						<div class="nav-collapse collapse "  role="navigation">
							<jdoc:include type="modules" name="menu" style="custom" />
						</div>
					<?php endif; ?>
				  
	            <!--/.nav-collapse -->
	          </div><!-- /.navbar-inner -->
	        </div><!-- /.navbar -->
	    </nav>
	<?php elseif (!$detect->isMobile() || $detect->isTablet() ) : //CG Mobile->Tablets ?>
			<div class="clear-phone-logo span4">
				<a class="logo animsition-link" href="<?php echo $this->baseurl; ?>">
					<?php echo $logo; ?>
					<?php if ($this->params->get('sitedescription')) : ?>
						<?php echo '<div class="site-description">' . htmlspecialchars($this->params->get('sitedescription')) . '</div>'; ?>
					<?php endif; ?>
				</a>
			</div>	
			<div class="mm-btn">
		        <a href="#menu" class="btn-menu">
		            <div id="toggle" class="button_container">
		                <span class="top"></span>
		                <span class="middle"></span>
		                <span class="bottom"></span>
		            </div>
		        </a>
		    </div>
			<nav id="menu">
				<div class="menu-mobile"  role="navigation">
					<jdoc:include type="modules" name="menu-mobile" style="custom" />
				</div>
			</nav>
	<?php else: //CG Mobile -> Smartphones ?>
			<div class="clear-phone-logo-mobile span12">
				<a href="<?php echo $this->baseurl; ?>">
					<img class="pull-left" src="images/logo-mobil.jpg" alt="IPL Logo Mobil" title="IPL Logo" />
				</a>
				<?php $regExp = '/\bis_home/';
					if(preg_Match($regExp, $body_class ) ) {$lg_header = 'h1 class="pull-left"'; } else {$lg_header = 'h2 class="pull-left"'; } ?>
					<<?=$lg_header?>>
						<a class="logo lg-mobile animsition-link" href="<?php echo $this->baseurl; ?>">
							 <span>Institut f&uuml;r</span><br/>Produktions<br /><span>management</span><br />und Logistik
						</a>
					</<?=$lg_header?>>
			</div>
			<div class="mm-btn">
		        <a href="#menu" class="btn-menu">
		            <div id="toggle" class="button_container">
		                <span class="top"></span>
		                <span class="middle"></span>
		                <span class="bottom"></span>
		            </div>
		        </a>
		    </div>
			<nav id="menu">
				<div class="menu-mobile"  role="navigation">
					<jdoc:include type="modules" name="menu-mobile" style="custom" />
				</div>
			</nav>
	<?php endif; ?>
