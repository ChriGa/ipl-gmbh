<?php
/**
 * @author   	cg@089webdesign.de
 */
 

defined('_JEXEC') or die;
//include system
include_once(JPATH_ROOT . "/templates/" . $this->template . '/lib/system.php');
//include template Functions CG
include_once(JPATH_ROOT . "/templates/" . $this->template . '/template_functions.php');
//
?>
<!DOCTYPE html>
<html lang="De-de">
<head>
	<?php 
	// including head
	include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/head.php');
	?>
 
</head>

<body id="body" class="site <?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. $body_class; print ($detect->isMobile()) ? "mobile" : "desktop";
?>">
	<!-- Body -->
		<div class="<?php print (!$detect->isMobile()) ? "container" : "container-fluid"; ?> site_wrapper">
			<?php			
				
				// including header
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/header.php');
				?>
			<section id="menu-section">
				<?php			
				// including header
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/menu.php');
				?>
			</section>
			<section id="inner-wrapper" class="row-fluid">
			<?php
				// including slider
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/slider.php');

				// including breadcrumb
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/breadcrumbs.php');				
										
				// including content
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/content.php');
				
				// including bottom
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom.php');								
				?>
			</section>
			<?php
				// including footer
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/footer.php');				
				
				?>					
			
		</div>
	<div class="scroll-top-wrapper ">
		<span class="scroll-top-inner">
			<span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
		</span>
	</div>
	
	<jdoc:include type="modules" name="debug" style="none" />

<script type="text/javascript">
/*scrollToTop Button*/
 	jQuery(function(){	 
		jQuery(document).on( 'scroll', function(){
	 
			if (jQuery(window).scrollTop() > 100) {
				jQuery('.scroll-top-wrapper').addClass('show');
			} else {
				jQuery('.scroll-top-wrapper').removeClass('show');
			}
		});	 
		jQuery('.scroll-top-wrapper').on('click', scrollToTop);
	});	 
	function scrollToTop() {
		verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
		element = jQuery('body');
		offset = element.offset();
		offsetTop = offset.top;
		jQuery('html, body').animate({scrollTop: offsetTop}, 500, 'linear');
	}

</script>

<?php if(!$detect->isMobile() ) : ?>
	<script>
		(function() {
			[].slice.call(document.querySelectorAll('.menu')).forEach(function(menu) {
				var menuItems = menu.querySelectorAll('.menu__link'),
					setCurrent = function(ev) {
						//ev.preventDefault();

						var item = ev.target.parentNode; // li

						// return if already current
						if (classie.has(item, 'menu__item--current')) {
							return false;
						}
						// remove current
						classie.remove(menu.querySelector('.menu__item--current'), 'menu__item--current');
						// set current
						classie.add(item, 'menu__item--current');
					};

				[].slice.call(menuItems).forEach(function(el) {
					el.addEventListener('click', setCurrent);
				});
			});
		})(window);
	</script>
<?php else : ?>
	<?php // mmenu ?>
	<script type="text/javascript">
		var $menu = jQuery('#menu');
		var $btnMenu = jQuery('.btn-menu');

		 jQuery(document).ready(function( $ ) {
		    jQuery("#menu").mmenu({
		       "extensions": [
		          "effect-menu-zoom",
		          "effect-panels-zoom",
		          "pagedim-black",
		          "pageshadow"
		       ],
		       "counters": true,
		       navbar: {
		             title: "IPL-GmbH Menü"       	
		       },
		       "navbars": [
		          {
		             "position": "bottom",
		             "content": [
		                "<a class='' href='/downloads.html'>Downloads</a>",
		                "<a class='' href='/impressum.html'>Impressum</a>",
		                "<a class='' href='/datenschutz.html'>Datenschutz</a>",
		                "<a class='' href='/links.html'>Links</a>"
		             ]
		          }
		       ]
		    });
		 	$menu.find( ".mm-next" ).addClass("mm-fullsubopen");
		 });
	</script>
<?php endif; ?>
</body>
</html>
