<?php
// inject jquery if joomla 2.5 to prevent js errors
jimport('joomla.version');
$version = new JVersion();
$version = $version->getShortVersion();

if(version_compare($version, '3.0', '<=')){
    JFactory::getDocument()->addScript($gantry->templateUrl.'/js/jq.min.js');
}
?>
<div class="template-preview">
	<img src="<?php echo $gantry->templateUrl;?>/template-thumb-big.png" style="max-width:323px;" />
	<h2>Key Features</h2>
	<ul>
		<li>Responsive Design supporting Phone, Tablets and Desktops</li>
                <li>Mega Menu</li>
                <li>Extensive Custom Colors Options</li>
		<li>Responsive Full Width Slider</li>
                <li>Rich Set of Iconic Icons</li>
		<li>Flexible widgets for template customization</li>
		<li>Full extensible framework architecture</li>
		<li>XML driven and with overrides for unprecedented levels of customization</li>
		<li>Per menu-item level of control over any configuration option with inheritance</li>
                <li>And many more!</li>
	</ul>
</div>
<div class="template-description">
	<h1>WorldBiz <span class="g4-version"><?php echo "1.5";//$gantry->_template->getVersion();?></span></h1>
	<h2>a Gantry based Joomla! template</h2>

	<p>The WorldBiz template is a clean modern responsive business design with stunning features such as mega menu, full color control, responsive full width slider, iconic buttons and many more.</p>

        <p>WorldBiz is made by Crosstec. For updates of this template frequently visit <a href="http://crosstec.de/en/joomla-templates.html">http://crosstec.de</a></p>
        
	<h2>WorldBiz is built upon Gantry. What is the Gantry Framework?</h2>

	<p>Gantry is a sophisticated framework with the sole intention of being the best platform to build a solid theme with.
	    Gantry takes all the lessons learned during the development of many RocketTheme Joomla templates and WordPress
	    Themes and distills that knowledge into a single super-flexible framework that is easy to configure, simple to
	    extend, and powerful enough to handle anything we want to throw at it.</p>

	<p>Get more help and find out more at <a href="http://www.gantry-framework.org/documentation/joomla">http://www.gantry-framework.org</a></p>

</div>
