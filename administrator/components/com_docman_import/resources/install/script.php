<?php
/**
 * @package    DOCman
 * @copyright   Copyright (C) 2011 - 2014 Timble CVBA (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

class com_docman_importInstallerScript
{
    public function postflight($type, $installer)
    {
        if ($type === 'discover_install' || !class_exists('ComExtmanModelEntityExtension')) {
            return;
        }

        $path = $installer->getParent()->getPath('source').'/extensions/plg_system_docman_redirect';
        $plugin = array(
            'type'    => 'plugin',
            'element' => 'docman_redirect',
            'folder'  => 'system'
        );
        $plugin_exists = ComExtmanModelEntityExtension::getExtensionId($plugin);

        $instance = new JInstaller();
        if ($instance->install($path))
        {
            // Only auto-enable if the plugin was just installed
            if (!$plugin_exists)
            {
                $extension_id = ComExtmanModelEntityExtension::getExtensionId($plugin);

                $db = JFactory::getDBO();
                $db->setQuery('UPDATE #__extensions SET enabled = 1 WHERE extension_id = '.(int)$extension_id);
                $db->query();
            }
        }
    }
}