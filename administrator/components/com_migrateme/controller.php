<?php
/* -------------------------------------------
Component: com_MigrateMe
Author: Barnaby V. Dixon
Email: barnaby@php-web-design.com
Copywrite: Copywrite (C) 2013 Barnaby Dixon. All Rights Reserved.
License: http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
---------------------------------------------*/
defined('_JEXEC') or die( 'Restricted access' );
jimport('joomla.application.component.controller');
if(!class_exists('JControllerLegacy')) {
    class JControllerLegacy extends JController {
        function __construct() {
            parent::__construct();
        }
    }
}

class MigrateMeController extends JControllerLegacy {
    
	function __construct( $default = array()) {
		parent::__construct( $default );
	}
    
    function config() {
        JRequest::checkToken() or jexit( 'Bad token. Please try again' );
        $this->setRedirect( 'index.php?option=com_migrateme&view=config');
    }
    
   	function runAjax() {
        if(isset($_GET['action'])) $action = htmlentities($_GET['action']);
        require_once('resources/helpers/migratemetasks.class.php');
        $x = new migrateMeTasks;
        $x->$action();
	}
    
   	function uploadFile() {
        require_once('resources/helpers/migrateme.class.php');
        $x = new migrateMe;
        $x->uploadFile();
	}
    
    function save() {
        JRequest::checkToken() or jexit( 'Unable to save your settings - bad token. Please try again' );

        //INITIALISE PARAMETERS
        $params = JComponentHelper::getParams('com_migrateme');
        
        //SET TIMEOUT PARAMETER
        $speed = (int) $_POST['mm_speed'];
        $over = (int) $_POST['mm_overwrite'];
        $aliases = (int) $_POST['mm_aliases'];
        $params->set('speed', $speed);
        $params->set('overwrite',$over);
        $params->set('aliases',$aliases);

        $db = JFactory::getDBO();
        $myparams = (string)$params;
        
        //BUILD THE QUERY
        $query = "UPDATE `#__extensions` SET `params` = '{$myparams}' WHERE `type` = 'component' AND `element` = 'com_migrateme' LIMIT 1";
		$db->setQuery($query);
		$db->query();
        
        //REDIRECT
		$msg = "Your configuration has been saved";
        $this->setRedirect( 'index.php?option=com_migrateme', $msg);
    }
    
    function cancel() {
        JRequest::checkToken() or jexit( 'Bad token. Please try again' );
        $this->setRedirect( 'index.php?option=com_migrateme');
    }

	function display ($cachable = false, $urlparams = false) {
        $view = JRequest::getCmd('view');
        if($view === "config") JRequest::setVar('view', 'config');
        else JRequest::setVar('view', 'default');
		parent::display();
	}
}