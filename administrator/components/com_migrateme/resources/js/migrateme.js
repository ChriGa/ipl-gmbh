jQuery(document).ready(function() {
    checkAjax();
    jQuery('#file_upload').uploadifive({
        'auto'          :true,
        'removeCompleted':true,
        'fileSizeLimit' : 0,
        'queueID'       : 'queue',
        'buttonText'    : 'Select file',
        'uploadScript'  : 'index.php?option=com_migrateme&task=uploadFile',
        'onUploadComplete': function(file, data) {
            if(data.substr(0,1) !== '1') {
                alert(data);
            }
            else {            
                unpackFile();
            }
        },
        'onFallback'   : function() {
            jQuery('#mm_import').hide();
            jQuery('#mm_html5').show();
        }
    });

    jQuery('#selectAll').click(function() {
        var checked = (!jQuery(this).attr('checked')) ? 0 : 1;
        jQuery('input.selectTable').each(function(a,b) {
            if(checked === 0) jQuery(this).removeAttr('checked');
            else jQuery(this).attr('checked', true);
        });
    });
    jQuery('#startMigration').click(function() {
        jQuery('#startMigration').hide();
        migrate();
    });
    jQuery('#mm_ok').click(function() {
        jQuery('#mm_taboo').fadeOut();
        jQuery('#mm_overcast').fadeOut();
    });
    jQuery('.showMe').click(function() {
        alert(jQuery(this).html());
    });
});

var resume = 0;
var step = 0;

function resumeMigration() {
    resume = 1;
    readTables();
}

function checkAjax() {
    jQuery.ajax({
        url: 'index.php?option=com_migrateme&task=runAjax&format=raw&action=checkAjax',
        dataType: 'json',
        cache: false,
        success: function(json) {
            if(json.error === 0) {
                jQuery('#ajax-check-img').removeClass().addClass('status-1');
                jQuery('#ajax-check-text').html(json.text);
            } else {
                jQuery('#ajax-check-img').removeClass().addClass('status-0');
                jQuery('#ajax-check-text').html('<a href="index.php?option=com_migrateme&amp;task=runAjax&amp;format=raw&amp;action=checkAjax" target="_blank">AJAX not enabled - click to test</a>');
            }
        },
        error: function() {
            jQuery('#ajax-check-img').removeClass().addClass('status-0');
            jQuery('#ajax-check-text').html('<a href="index.php?option=com_migrateme&amp;task=runAjax&amp;format=raw&amp;action=checkAjax" target="_blank">AJAX not enabled - click to test</a>');
        }
    });
}

function startFresh() {
    jQuery('.ajax').show();
    jQuery.ajax({
        url: 'index.php?option=com_migrateme&task=runAjax&format=raw&action=startFresh',
        dataType: 'json',
        cache: false,
        success: function(json) {
            jQuery('.ajax').hide();
            showStep('1');
        },
        error: function() {
            jQuery('.ajax').hide();
            showStep('1');
        }
    });
}

function unpackFile() {
    showStep('2');
    jQuery('.ajax').show();
    jQuery.ajax({
        url: 'index.php?option=com_migrateme&task=runAjax&format=raw&action=unpackFile',
        dataType: 'json',
        cache: false,
        success: function(json) {
            jQuery('#mm_step2 .ajax').append('.');
            if(json.error !== 0) {
                alert(json.error);
            } else {
                saveTables();
            }
        },
        error: function() {
            jQuery('.ajax').hide();
            jQuery('.mm_step').hide();
            jQuery('#mm_step1').fadeIn();
        }
    });
}

function saveTables() {
    showStep('2');
    jQuery('.ajax').show();
    jQuery.ajax({
        url: 'index.php?option=com_migrateme&task=runAjax&format=raw&action=saveTables',
        dataType: 'json',
        cache: false,
        success: function(json) {
            if(json.error !== 0) {
                alert(json.error);
            } else if(json.point === 'saveTables') {
                jQuery('#mm_step2 .ajax').append('.');
                saveTables();
            } else {
                readTables();
            }
        },
        error: function() {
            jQuery('.ajax').hide();
            jQuery('.mm_step').hide();
            jQuery('#mm_step1').fadeIn();
        }
    });
}

function showStep($id) {
    if($id !== step) {
        step = $id;
        jQuery('.mm_step').hide();
        jQuery('#mm_step'+$id).fadeIn();
    }
}

function readTables() {
    showStep('2');
    jQuery('.ajax').show();
    jQuery('#mm_step2_content').hide();
    jQuery('#mm_select').empty();
    jQuery.ajax({
        url: 'index.php?option=com_migrateme&task=runAjax&format=raw&action=readTables',
        type: 'POST',
        dataType: 'json',
        cache: false,
        success: function(json) {
            jQuery('.ajax').hide();
            if(json.error.length > 0) {
                alert(json.error);
                showStep('1');
                return;
            }
            jQuery(json.tables).each(function (n,table) {
                var name = table[0];
                var id = table[1];
                var count = table[2];
                var string = '<input type="checkbox" name="table[]" class="selectTable" value="'+name+'" id="table-'+id+'" count="'+count+'" /><label for="table-'+id+'" class="styled" id="label-'+id+'">'+name+'</label><br />';
                jQuery('#mm_select').append(string);
                jQuery('#mm_step2_content').fadeIn();
                jQuery('#mm_step3').fadeIn();
            });
            if(json.taboo !== 0) {
                jQuery('#mm_taboo').fadeIn();
                jQuery('#mm_overcast').fadeIn();
            }
        }, error:function(a,b,c) {
            alert(c+"\n\nError details:\nStatus:"+a.status+"\nError response:"+a.responseText+"\n\nIf this error continues, please check Troubleshooting in the User Guide for more information, or contact support@php-web-design.com for assistance. Thank you.");
        }
    });
}
function migrate() {
    jQuery('#mm_step3').show();
    jQuery('#mm_step3 #startMigration').hide();
    jQuery('#mm_step3 h3').html('Step 3: Migration in progress...');
    jQuery('#mm_step3 .ajax').show();
    jQuery('#mm_step3 .complete').hide();
    var tables = jQuery('input.selectTable:checked');
    if(tables.length < 1) {
        alert('Please select one or more tables to migrate');
        showStep('2');
        return;
    }
    migrateTable(0,1);
}

function migrateTable(n,fresh) {
    var tables = jQuery('input.selectTable:checked');
    var table = tables.eq(n);
    var tabname = table.val();
    var id = jQuery(table).attr('id');
    id = id.replace('table-','');
    jQuery('#label-'+id).removeClass().addClass('mm_processing styled');
    if(fresh === 1) {
        jQuery('#mm_step3 .ajax').html('Importing '+tabname+': 0% done');
        jQuery('#label-'+id).html(tabname+': Processing 0%');
    }
    jQuery.ajax({
        url: 'index.php?option=com_migrateme&task=runAjax&format=raw&action=migrateData',
        type: 'POST',
        dataType: 'json',
        cache: false,
        data: {table:tabname,fresh:fresh,resume:resume},
        success: function(json) {
            resume = 0;
            if(json.done === 1) {
                jQuery('#label-'+id).html(tabname+': 100% complete');
                jQuery('#label-'+id).removeClass().addClass('mm_done styled');
                n++;
                if(n < tables.length) {
                    migrateTable(n,1);
                }
                else finish();
            } else {
                var total = jQuery(table).attr('count');
                var pc = 100;
                if(total > 0) {
                    var rows = json.rows;
                    var pc = ((total-rows) / total) * 100;
                    pc = pc.toPrecision(3);
                }
                jQuery('#mm_step3 .ajax').html('Importing '+tabname+': '+pc+'% done');
                jQuery('#label-'+id).html(tabname+': Processing '+pc+'%');
                migrateTable(n,0);
            }
        }, error:function (x,y,z) {
            alert(z+"\n\nError details:\nStatus:"+x.status+"\nError response:"+x.responseText+"\n\nIf this error continues, please check Troubleshooting in the User Guide for more information, or contact support@php-web-design.com for assistance. Thank you.");
        }
    });
}

function finish() {
    jQuery('#mm_step3 .ajax').html('Finalising and cleaning tables');
    jQuery.ajax({
        url: 'index.php?option=com_migrateme&task=runAjax&format=raw&action=finishMigration',
        type: 'POST',
        dataType: 'json',
        cache: false,
        success: function(json) {
            jQuery('#mm_scroll').hide();
            showStep(4);
        }, error:function (x,y,z) {
            alert(z+"\n\nError details:\nStatus:"+x.status+"\nError response:"+x.responseText+"\n\nIf this error continues, please check Troubleshooting in the User Guide for more information, or contact support@php-web-design.com for assistance. Thank you.");
        }
    });
}