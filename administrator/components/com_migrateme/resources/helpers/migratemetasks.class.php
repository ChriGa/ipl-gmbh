<?php
/* -------------------------------------------
Component: com_MigrateMePLUS
Author: Barnaby V. Dixon
Email: barnaby@php-web-design.com
Copywrite: Copywrite (C) 2013 Barnaby Dixon. All Rights Reserved.
License: http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
---------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

require_once('migrateme.class.php');

error_reporting(E_ERROR);
ini_set('display_errors', 'On');

//TIMEZONE FIX
if(!ini_get('date.timezone')) @date_default_timezone_set('UTC');
//FIX FOR MYSQLI EXCEPTION BUG
if(function_exists('mysqli_report')) @mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

class migrateMeTasks extends migrateMe {

    public function __construct() {
        parent::__construct();
                
        //CHECK SAFE MODE
        if($this->compatibility['safe_mode'] !== 1) $this->error('Unable to continue. Please set safe mode to OFF in your php.ini');
        
        //CHECK THE MEMORY
        if($this->compatibility['memory'] < 64) $this->error('Insufficient memory. Please increase your memory limit to 128Mb or above in your php.ini');

        //TIME DELAY MANAGEMENT
        $this->time_start = microtime(true);        
    }
    
    public function __destruct() {
        parent::__destruct();
        $this->saveJson();
    }
    
    public function startFresh() {
        
        //DELETE OLD MIGRATION TABLES
        $array = $this->getTables();
        if(count($array)>0) foreach($array as $a) {
            $query = "DROP TABLE ".$this->db->quoteName('mm_'.$a);
            $this->db->setQuery($query);
            try {
                $result = $this->db->query();
            }
            catch (Exception $e) {
                $this->log('Unable to remove old migration tables - please ensure you only migrate the data you need.');
            }
        }
        //DELETE UPLOADED DATABASE
        if(file_exists($this->dbFile)) @unlink($this->dbFile);
        if(file_exists($this->gzFile)) @unlink($this->gzFile);
        
        //RESET THE LOG
        $this->truncateLog();
                
        $this->json['text'] = 'ok';
    }

    public function checkAjax() {
        $this->json['text'] = 'AJAX enabled';
    }

    public function unpackFile() {
        
        $this->truncateLog();
        
        //DELETE OLD MIGRATION TABLES
        $array = $this->getTables();
        if(count($array)>0) foreach($array as $a) {
            $query = "DROP TABLE ".$this->db->quoteName('mm_'.$a);
            $this->db->setQuery($query);
            try {
                $result = $this->db->query();
            }
            catch (Exception $e) {
                $this->log('Unable to remove old migration tables - please ensure you only migrate the data you need.');
            }
        }
        
        if(file_exists($this->dbFile.'.gz')) {
            $gzFile = $this->dbFile.'.gz';
            $gzip = (function_exists("readgzfile")) ? 1 : 0;
            if($gzip === 0) $this->error('Please enable GZip to upload a GZip compressed archive.');
            if(!$handle = gzopen($gzFile, "r")) $this->error('Unable to open uploaded file');
            if(!$newhandle = fopen($this->dbFile,'w+')) $this->error('Unable to save uploaded file. Please check that /administrator/components/com_migrateme/'.$this->dbFile.' exists and is writable.');
            while (!gzeof($handle)) {
                fwrite($newhandle, gzread($handle, 10240));
            }
            fclose($newhandle);
            gzclose($handle);
        }
        if(!file_exists($this->dbFile)) {
            $this->error('The database backup could not be saved at /administrator/components/com_migrateme/'.$this->dbFile.'. Please check that the server can write to the folder.');
        }
        $this->json['point'] = 'getTables';
        $this->json['text'] = 'Analysing your database';
    }
    
    public function saveTables() {
        
        $handle = fopen($this->dbFile, 'r+');
        if($handle === FALSE) {
            $this->error('Unable to open '.$this->dbFile.'. Please ensure the database file uploaded correctly and is accessible by the server.');
        }
        $speed = 1024*256; //256Kb chunks
        $sql = '';
        $sep = ";\r\n";
        $timeout = ($this->speed > 2) ? $this->speed : 2;

        //READ THE DATABASE FILE & SAVE AS TEMPORARY TABLES        
        while(!feof($handle)) {
            $sql .= fread($handle, $speed);
            if(!isset($separator)) {
                $separator = $sep;
                foreach(array(";\r\n",";\n",";\r") as $a) {
                    if(strpos($sql,$a)!== FALSE) {
                        $separator = $a;
                        break;
                    }
                }
            }
            if(substr_count($sql, $separator) > 1) {
                $queries = explode($separator, $sql);
                if(count($queries)>0) {
                    if(!feof($handle)) $sql = array_pop($queries);
                    foreach($queries as $q) {
                        $tabs = array();
                        if(strlen(trim($q))<5) continue;
                        $pos = strpos($q, '(');
                        $tables = substr($q, 0, $pos);
                        $tables = explode('`', $tables);
                        if(count($tables)<2) continue;
                        foreach($tables as $n=>$t) {
                            $i = $n+1;
                            if($i%2 !== 0) continue;
                            if(!in_array($t, $tabs)) {
                                $tabs[] = $t;
                                $q = str_replace($t, 'mm_'.$t, $q);
                            }
                        }
                        $this->db->setQuery($q);
                        try {
                            $result = $this->db->query();
                        }
                        catch (Exception $e) {
                            //$this->error('Unable to process tables. Please ensure you have included both STRUCTURE and CONTENT in your database backup\nQuery: '.$q);
                            $this->log('WARNING: Unable to set up temporary table. DB error is: '.$this->db->getErrorMsg().'\n\nFull Query is: '.$q).'\n\n';
                        }
                    }
                    if($this->checkTime() > $timeout) break;
                }
            }
        }

        if(!feof($handle)) {
            $newhandle = fopen($this->dbFile.'x','w+');
            fwrite($newhandle,$sql);
            while(!feof($handle)) {
                $sql = fread($handle,$speed);
                fwrite($newhandle,$sql);
            }
            fclose($handle);
            fclose($newhandle);
            unlink($this->dbFile);
            rename($this->dbFile.'x',$this->dbFile);
            
            $this->json['point'] = 'saveTables';
        } else {
            $this->json['point'] = 'readTables';
            fclose($handle);
            //DELETE UPLOADED DATABASE
            if(file_exists($this->dbFile)) unlink($this->dbFile);
            if(file_exists($this->gzFile)) unlink($this->gzFile);
        }
    }
    
    public function readTables() {
        
        //GET TEMPORARY TABLE NAMES
        $tables = $this->getTables();
        $this->json['tables'] = array();
        $this->json['taboo'] = 0;
        
        if(count($tables)<1) $this->error('This database cannot be read - it has no valid Joomla tables');
        
        $error = 0;
        
        foreach($tables as $id=>$table) {
            
            $query = "SELECT COUNT(*) AS `c` FROM `mm_{$table}`";
            $this->db->setQuery($query);
            $count = (int) $this->db->loadResult();

            $this->json['tables'][] = array($table,$id,$count);

            $pos = strpos($table, '_');
            $temp = substr($table, 0, $pos);
            if(isset($mytable) && $mytable !== $temp) {
                $this->json['taboo'] = 1;
            }
            $mytable = $temp;
            if($table === $mytable.'_assets') $error = 1;
        }
        if($error === 1) {
            if($this->json['taboo'] !== 1) $this->error('This is not a valid Joomla 1.5 database.');
        }
    }

    public function migrateData() {
        parent::__construct();
        $this->table = '';
        $this->json['done'] = 0;
        $this->json['rows'] = 0;
        if(isset($_POST['table'])) $this->origtable = @$_POST['table'];
        else {
            $this->error('No table, or incorrect table, selected. Please try again');
            return;            
        }
        if(isset($_POST['resume'])) $this->resume = (int) @$_POST['resume'];
        if(isset($_POST['fresh'])) $this->fresh = (int) @$_POST['fresh'];
        $this->savetable = $this->origtable;
        if(($mypos = strpos($this->savetable, '_')) !== FALSE) $this->savetable = substr($this->origtable,$mypos+1);

        if(!$this->getTable()) {
            $this->json['done'] = 1;
        } else {
            if($this->fresh === 1) $this->log("Migrating data from {$this->tab}");
            $timeout = ($this->speed > 2) ? $this->speed : 2;
            do {
                if(array_key_exists($this->tab,$this->map)) {
                    $this->runData();
                } else {
                    $this->cloneData();
                }
                $query = "SELECT COUNT(*) as c FROM `mm_{$this->origtable}`";
                $this->db->setQuery($query);
                $rows = (int) $this->db->loadResult();
                $this->json['rows'] = $rows;
                if($rows === 0) {
                    $this->json['done'] = 1;
                } else {
                    $this->json['done'] = 0;
                }
            } while ($this->json['done'] === 0 && $this->checkTime() < $timeout);
        }
        
        if($this->json['done'] === 1) {
            $this->postMigrationUpdates();
            //REMOVE THIS TABLE
            $query = "DROP TABLE ".$this->db->quoteName('mm_'.$this->origtable);
            $this->db->setQuery($query);
            try {
                $result = $this->db->query();
            }
            catch (Exception $e) {
                $this->log('Unable to remove the temporary table mm_'.$t);
            }
        }
    }
    
    public function cloneData() {
        
        $count = 500;

        $fields = array();
        $query = "SHOW COLUMNS FROM `mm_{$this->origtable}`";
        try {
            $this->db->setQuery($query);
            $cols = $this->db->loadAssocList();
            if(count($cols)>0) foreach($cols as $c) {
                $fields[] = $c['Field'];
            }
        }
        catch (Exception $e) {
            //Do something
        }
        if(count($fields)>0) $fields = '(`'.implode($fields,'`,`').'`)';
        else $fields = '';
        
        $query = "INSERT {$this->table} {$fields} SELECT * FROM `mm_{$this->origtable}` LIMIT 0,{$count}";
        try {
            $this->db->setQuery($query);
            $this->db->query();
        }
        catch (Exception $e) {
            $msg = 'WARNING! Unable to clone data from '.$this->origtable.' Error message: '.$e->getMessage();
            $this->log($msg);
        }
        
        try {
            $query = "DELETE FROM mm_{$this->origtable} LIMIT {$count}";
            $this->db->setQuery($query);
            $this->db->query();
        }
        catch (Exception $e) {
            $msg = 'WARNING! Unable to delete temporary data from '.$this->origtable.'. Please check your database user has DELETE permissions.';
            $this->log($msg);
        }
    }

    public function runData() {
        
        $ordering = "";
        if($this->tab === 'menu') {
            $ordering .= " ORDER BY `parent` ASC, `ordering` DESC  "; //`sublevel` DESC ,  `id` ASC "; //
        }
        elseif($this->tab === 'categories') {
            $ordering .= "ORDER BY `ordering` DESC ,  `id` ASC ";
        }
        $query = "SELECT * FROM `mm_{$this->origtable}` {$ordering} LIMIT 0, 1";
        $this->db->setQuery($query);
        try {
            $queries = $this->db->loadAssocList();
        }
        catch (Exception $e) {
            $this->log('Unable to import '.$this->savetable.' - data has already been imported.');
            $this->json['done'] = 1;
            return;
        }

        $count = count($queries);
        
        //LOOP THROUGH DATA AND SAVE TO DATABASE
        if($count>0) {
            foreach($queries as $n=>$query) {
                if(!$query = $this->mapFields($query)) continue;
                
                //REMOVE ID IF WE ARE ADDING TO CURRENT DATA
                if((int)$this->overwrite === 2) unset($query['id']);
                
                if(in_array($this->tab, $this->asset_tables)) $this->insertAssets($query);
                else $this->insertContent($query);
            }
            //DELETE MIGRATED ROWS
            try {
                $query = "DELETE FROM mm_{$this->origtable} {$ordering} LIMIT {$count}";
                $this->db->setQuery($query);
                $this->db->query();
            }
            catch (Exception $e) {
                $msg = 'WARNING! Unable to delete temporary data from '.$this->origtable.'. Please check your database user has DELETE permissions.';
                $this->log($msg);
            }
        }
    }

    function finishMigration() {

        //UPDATE THE CATEGORY PARENTS WITH SECTION IDs
        $query = 'SELECT '.
            $this->db->quoteName('id')
            .','.$this->db->quoteName('note')
            .','.$this->db->quoteName('alias')
            .' FROM '.$this->db->quoteName('#__categories');
        $this->db->setQuery($query);
        $res = $this->db->loadAssocList();
        if(count($res)>0) foreach($res as $r) {
            
            $note = $r['note'];

            //GET THE CATEGORY IDs
            $pcre = '#c[0-9]+#';
            if(preg_match($pcre,$note,$matches)) {
                $cat = str_replace('c','',$matches[0]);
                $categories[$cat] = $r['id'];
            }
            
            //GET THE SECTION IDs
            $pcre = '#sid[0-9]+#';
            if(preg_match($pcre,$note,$matches)) {
                $sec = str_replace('sid','',$matches[0]);
                $section[$sec] = $r['id'];
            }
        }
        
        //UPDATE THE SECTION FOR EACH CATEGORY!
        $this->table = $this->db->quoteName('#__categories');
        if(count($res)>0) foreach($res as $r) {
            $note = $r['note'];
            $pcre = '#s[0-9]+#';
            if(preg_match($pcre,$note,$matches)) {
                $sec = substr($matches[0],1);
                $sec = (isset($section[$sec])) ? (int) $section[$sec] : 1;
                if($sec < 1) $sec = 1;
                $alias = $this->fixAlias($r['alias'],$r['id']);
                $query = "UPDATE `#__categories` SET `alias` = '{$alias}', `parent_id` = '{$sec}', `level` = '2' WHERE `id` = '{$r['id']}' LIMIT 1";
                $this->db->setQuery($query);
                try {
                    $result = $this->db->query();
                }
                catch (Exception $e) {
                    $this->log("WARNING: Unable to update categories with section parent. Full MySql query: ".$query);
                }
            }
        }
        
        //REBUILD THE CATEGORY TABLE WITH NEW PARENTS
        $row = JTable::getInstance('category');
        $row->rebuild();
        
        //UPDATE THE MENU TABLE WITH CORRECT IDs
        $query = 'SELECT '.$this->db->quoteName('id')
            .','.$this->db->quoteName('note')
            .','.$this->db->quoteName('link')
            .' FROM '.$this->db->quoteName('#__menu');
        $this->db->setQuery($query);
        $res = $this->db->loadAssocList();
        if(count($res)>0) foreach($res as $r) {
            $link = $r['link'];
            
            if(strpos($link,'com_content') || strpos($link,'view=section')) {
                $link = str_replace('view=section','view=category',$link);
                $pcre = '#&id=[0-9]+#';
                if(preg_match($pcre,$link,$matches)) {
                    $id = $matches[0];
                    $id = (int) str_replace('&id=','',$id);
                    if(isset($section[$id])) $link = str_replace('&id='.$id, '&id='.$section[$id],$link);
                    $query = "UPDATE `#__menu` SET `link` = '{$link}' WHERE `id` = '{$r['id']}' LIMIT 1";
                    $this->db->setQuery($query);
                    try {
                        $result = $this->db->query();
                    }
                    catch (Exception $e) {
                        $this->log("WARNING: Unable to update menu with fixed link. Full MySql query: ".$query);
                    }
                }
            }
            
            //UPDATE CONTENT ITEMS LINKS WITH CORRECT MENU IDS
            $note = str_replace('m','',$r['note']);
            if((int)$note < 1) continue;
            $orig = 'Itemid='.$note;
            $rep = 'Itemid='.$r['id'];
            $query = "UPDATE `#__content` SET `introtext` = REPLACE(`introtext`, '$orig','$rep'), `fulltext` = REPLACE(`fulltext`, '$orig','$rep');";
            try {
                $this->db->setQuery($query);
                $this->db->query();
            } catch (Exception $e) {
                $this->log("WARNING: Unable to update content links with updated Menu ID. Full MySql query: ".$query);
            }
        }

        //DELETE TEMPORARY NOTES FROM TABLES
        $query = "UPDATE ".$this->db->quoteName('#__categories')
            ." SET ".$this->db->quoteName('note')
            ." = ".$this->db->quote('');
        $this->db->setQuery($query);
        $this->db->query();
        $query = "UPDATE ".$this->db->quoteName('#__menu')
            ." SET ".$this->db->quoteName('note')
            ." = ".$this->db->quote('');
        $this->db->setQuery($query);
        $this->db->query();
        $query = "UPDATE ".$this->db->quoteName('#__modules')
            ." SET ".$this->db->quoteName('note')
            ." = ".$this->db->quote('');
        $this->db->setQuery($query);
        $this->db->query();
        
        //DELETE ANY REMAINING MM TABLES
        $tables = $this->getTables();
        foreach($tables as $t) {
            $query = "DROP TABLE ".$this->db->quoteName('mm_'.$t);
            $this->db->setQuery($query);
            try {
                $result = $this->db->query();
            }
            catch (Exception $e) {
                $this->log('Unable to remove the temporary table mm_'.$t);
            }
        }
        
        //SAVE JSON
        $this->json['end'] = 1;
    }
}