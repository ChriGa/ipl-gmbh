<?php
/* -------------------------------------------
Component: com_MigrateMePLUS
Author: Barnaby V. Dixon
Email: barnaby@php-web-design.com
Copywrite: Copywrite (C) 2013 Barnaby Dixon. All Rights Reserved.
License: http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
---------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class migrateMe {
    
    protected $_logFile = '';
    protected $_logHandle = NULL;
    protected $params = array();
    protected $max_upload = 0;
    protected $max_post = 0;
    protected $memory_limit = 0;
    protected $upload_limit = 0;
    protected $dbFile = '';
    protected $_fileTypes = array('txt', 'sql', 'gz');
    protected $json = array('point' => '', 'text' => 'Please wait...', 'error' => 0);
    protected $_timeout = 1;
    protected $_recursions = 10;
    protected $origtable = '';
    protected $resume = 0;
    protected $fresh = 0;
    protected $map = array(
            'banners' => array(
                'id'=>'$bid',
                'bid' => 0,
                'catid' => '%getCatId',                
                'imageurl' =>0,
                'date' => 0,
                'showBanner' => 0,
                'custombannercode' => 0,
                'editor' => 0,
                'tags' => 0,
                'state' => '%getState'
            ),
            'banner_clients' => array(
                'id' => '$cid',
                'cid' => 0,
                'editor' => 0,
                'state' => '%getState'
            ),
            'contact_details' => array(
                'imagepos' => 0
            ),
            'menu' => array(
                'title' => '&name',
                'pollid' => 0,
                'utaccess' => 0,
                'access' => '+1',
                'sublevel' => '+1',
                'level' => '&sublevel',
                'parent_id' => '&parent',
                'component_id' => '&componentid',
                'type' => '%fixMenuType',
                'language' => '!*',
                'id' => 0,
                'path' => '$alias',
                'params' => '%getJson',
                'lft' => '&ordering',
                //'ordering' => 0,
                'link' => '%getLink',
                'img' => '! '
            ),
            'modules' => array(
                'id' => 0,
                'numnews' => 0,
                'iscore' => 0,
                'control' => 0,
                'params' => '%getJson',
                'access' => '+1',
                'language' => '*',
                'module' => '%fixModule'
            ),
            'modules_menu' => array(
                'menuid' => '%getMenuId'
            ),
            'content' => array(
                'asset_id' => 0,
                'title_alias' => 0,
                'catid' => '%getCatId',
                'parentid' => 0,
                'sectionid' => 0,
                'mask' => 0,
                'access' => '+1',
                'featured' => '!0',
                'language' => '!*',
                'attribs' => '%getJson',
                'metadata' => '%getJson',
                'state' => '%getState'
            ),
            'categories' => array(
                'id'=>0,
                'asset_id' => 0,
                'parent_id' => '!1',
                'level' => '!1',
                'name' => 0,
                'section' => '%getSection',
                'extension' => '&section',
                'image' => 0,
                'image_position' => 0,
                'ordering' => 0,
                'count' => 'hits',
                'path' => '$alias',
                'access' => '+1',
                'language' => '!*',
                'params'=> '%getJson',
                'metadata'=> '%getJson',
                'created_user_id' => '%getUser'
            ),
            'newsfeeds' => array(
                'filename' => 0,
                'catid' => '%getCatId',                
                'metadata'=> '%getJson',
                'params'=> '%getJson'
            ),
            'users' => array(
                'usertype' => 0,
                'gid' => 0,
                'params'=> '%getJson'
            ),
            'tags' => array (
                'params'=> '%getJson'
            ),
            'weblinks' => array(
                'sid'=> 0,
                'catid' => '%getCatId',
                'created'=>'$date',
                'date'=>0,
                'state'=>'$published',
                'published'=>0,
                'approved'=>0,
                'archived'=>0,
                'params'=> '%getJson',
                'metadata'=> '%getJson',
                'created_by' => '%getUser',
                'state' => '%getState'
            ),
            'sections' => array(
                'id' => 0,
                'name' => 0,
                'image_position' => 0,
                'scope' => '%getScope',
                'extension' => '&scope',
                'ordering' => 0,
                'count' => 'hits',
                'params' => '%getJson',
                'parent_id' => '!1',
                'level' => '!1',
                'path' => '$alias',
                'access' => '+1',
                'metadata' => '%getJson',
                'language' => '!*',
                'version' => '!1',
                'created_user_id' => '%getUser'
            ),
            'kunena_categories' => array(
                'access' => '+1'
            ),
            'kunena_users' => array(
                'banned' => 'NULL',
            ),
            'k2_items' => array(
                'params' => '%getJson',
                'plugins' => '%getJson',
                'access' => '+1',
                'language' => '!*'
            ),
            'k2_categories' => array(
                'params' => '%getJson',
                'plugins' => '%getJson',
                'access' => '+1'
            )
        );
        
        protected $ignore_tables = array(
            'assets',
            'modules_menu',
            'components',
            'extensions',
            'session',
            'jupgrade_categories',
            'jupgrade_menus',
            'jupgrade_modules',
            'jupgrade_steps',
            'template_styles',
            'viewlevels'
        );
        
        protected $deprecated_tables = array(
            'core_acl_aro',
            'core_acl_aro_groups',
            'core_acl_aro_map',
            'core_acl_aro_sections',
            'core_acl_groups_aro_map',
            'core_log_items',
            'groups',
            'migration_backlinks',
            'plugins',
            'polls',
            'poll_data',
            'poll_date',
            'poll_menu',
            'stats_agent',
            'templates_menu'
        );
        
        protected $remap_tables = array(
            'banner' => 'banners',
            'bannerclient' => 'banner_clients',
            'bannertrack' => 'banner_tracks',
            'category' => 'categories',
            'sections' => 'categories'
        );

        protected $asset_tables = array(
            'categories',
            'sections',
            'menu'
        );
        
        protected $note_tables = array(
            'menu',
            'categories',
            'sections',
            'modules'
        );
        protected $prepend_tables = array(
            'categories'
        );
        
    protected $unexistingStructures = array(
        'weblinks' => 
            "CREATE TABLE IF NOT EXISTS `#__weblinks` (
              `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `catid` int(11) NOT NULL DEFAULT '0',
              `title` varchar(250) NOT NULL DEFAULT '',
              `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
              `url` varchar(250) NOT NULL DEFAULT '',
              `description` text NOT NULL,
              `hits` int(11) NOT NULL DEFAULT '0',
              `state` tinyint(1) NOT NULL DEFAULT '0',
              `checked_out` int(11) NOT NULL DEFAULT '0',
              `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
              `ordering` int(11) NOT NULL DEFAULT '0',
              `access` int(11) NOT NULL DEFAULT '1',
              `params` text NOT NULL,
              `language` char(7) NOT NULL DEFAULT '',
              `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
              `created_by` int(10) unsigned NOT NULL DEFAULT '0',
              `created_by_alias` varchar(255) NOT NULL DEFAULT '',
              `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
              `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
              `metakey` text NOT NULL,
              `metadesc` text NOT NULL,
              `metadata` text NOT NULL,
              `featured` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Set if link is featured.',
              `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
              `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
              `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
              `version` int(10) unsigned NOT NULL DEFAULT '1',
              `images` text NOT NULL,
              PRIMARY KEY (`id`),
              KEY `idx_access` (`access`),
              KEY `idx_checkout` (`checked_out`),
              KEY `idx_state` (`state`),
              KEY `idx_catid` (`catid`),
              KEY `idx_createdby` (`created_by`),
              KEY `idx_featured_catid` (`featured`,`catid`),
              KEY `idx_language` (`language`),
              KEY `idx_xreference` (`xreference`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;"
    );
    
    function __construct() {
        //CHECK UPLOAD LIMIT
        $this->max_upload = (int) ini_get('upload_max_filesize');
        $this->max_post = (int) ini_get('post_max_size');
        $this->memory_limit = (int) ini_get('memory_limit');
        $this->upload_limit = min($this->max_upload, $this->max_post, $this->memory_limit);
        $this->upload_limit = ($this->upload_limit * 1024 * 1024);
        
        //DATABASE UPLOADED FILE
        $this->dbFile = JPATH_COMPONENT.'/insert.sql';
        $this->gzFile = $this->dbFile.'.gz';
        $this->_logFile = JPATH_COMPONENT.'/log.txt';
        
        //INITIALISE PARAMETERS
        jimport('joomla.application.component.helper');
        $this->params = JComponentHelper::getParams('com_migrateme');
        $this->speed = $this->params->get('speed', 2);
        $this->aliases = $this->params->get('aliases', 0);
        $this->overwrite = $this->params->get('overwrite', '1');
        
        //CHECK COMPATIBILITY
        $this->checkCompatibility();
        

        //CONNECT TO DATABASE                
        if(!class_exists('JFactory')) $this->error('Unable to load framework');
        $mainframe = JFactory::getApplication('site');
        $mainframe->initialise();
        $this->db = JFactory::getDBO();
    }
    
    function __destruct() {
        if(isset($this->loghandle)) fclose($this->loghandle);
    }
       
    function log($msg = 1, $ret = 1) {
        $time = date('H:i:s');
        if($ret === 1) $msg .=' '.$time. "\r\n";
        if(NULL === $this->_logHandle) {
            if(!($this->loghandle = fopen($this->_logFile,'a'))) $this->error('Unable to create log file - aborting');
        }
        fwrite($this->loghandle,$msg);
    }
       
    function truncateLog() {
        if(NULL === $this->_logHandle) {
            if(!($this->loghandle = fopen($this->_logFile,'w+'))) $this->error('Unable to create log file - aborting');
        }
        ftruncate($this->loghandle,0);
    }
    
    function error($msg) {
        $this->json = array();
        $this->json['error'] = $msg;
        $this->log("WARNING: Error - {$msg}. Aborting import");
        die();
    }
    
    function checkTime() {
        $time_end = microtime(true);
        $diff = $time_end - $this->time_start;
        return $diff;
    }
    
    function saveJson() {
        $diff = $this->checkTime();
        $diff = $diff - ($this->speed * 1000000);
        if($diff < 0) usleep(abs($diff));
        die(json_encode($this->json));
    }
    
    function checkCompatibility() {
        
        //SAFE MODE
        $this->compatibility['safe_mode'] = (ini_get('safe_mode')) ? 0 : 1;

        //MEMORY
        $temp = (int) @ini_get('memory_limit');
        if($temp <= 128) {
            @ini_set('memory_limit','256M');
            $temp = (int) @ini_get('memory_limit');
        }
        $this->compatibility['memory'] = $temp;
        
        //TIMEOUT
        @set_time_limit(300);
        $temp = ini_get('max_execution_time');
        $this->compatibility['timeout'] = $temp;
        
        //CHECK UPLOAD LIMIT
        $temp = 0;
        $max_upload = (int) ini_get('upload_max_filesize');
        $max_post = (int) ini_get('post_max_size');
        $temp = min($max_upload, $max_post);
        $this->compatibility['upload_limit'] = $temp;
    }
        
    function getTables() {
		require_once( JPATH_CONFIGURATION.DS.'configuration.php' );
		$CONFIG = new JConfig();
		$name = $CONFIG->db;
        $name = addslashes($name);
		$query = "SHOW TABLES FROM `$name` LIKE ".$this->db->quote('mm\_%');
		$this->db->setQuery($query);
		$array = $this->db->loadObjectList();
        $tabarray = array();
        $prepend = array();
        foreach($array as $a=>$b) {
            foreach($b as $c) {
                $cat = substr($c, 3);
                $pos = strpos($cat, '_');
                $subtab = substr($cat,($pos+1));
                if(in_array($subtab,$this->prepend_tables)) $prepend[] = $cat;
                else $tabarray[] = $cat;
            }
        }
        if(count($prepend)>0) foreach($prepend as $a) array_unshift($tabarray,$a);
        return $tabarray;
    }

    function checkFileExists() {
        if(file_exists($this->dbFile) && filesize($this->dbFile)>1024) {
            return TRUE;
        } else if(file_exists($this->gzFile) && filesize($this->gzFile)>1024) {
            return TRUE;
        }
        return FALSE;
    }
    
    function checkPreviousMigration() {
        $array = $this->getTables();
        if(count($array)>0) return TRUE;
        return FALSE;
    }

    function uploadFile() {
        $this->truncateLog();
        if(!isset($_FILES['Filedata'])) $this->error('No file was uploaded. Please try again');
        if (!empty($_FILES['Filedata'])) {
            $error = $_FILES['Filedata']['error'];
            if($error === 0 && $_FILES['Filedata']['size'] >= $this->upload_limit) $error = 2;
            if((int)$error !== 0) {
                switch($error) {
                    case 1:
                    case 2:
                        $msg = "The backup file is larger than the max permitted upload size. Please adjust the upload_max_filesize directive in your php.ini file, or ask your server admins to do this for you.";
                        break;
                    case 3: $msg = "The backup file was only partially uploaded. Please try again.";
                        break;
                    case 4: $msg = "No file was uploaded. Please try again";
                        break;
                    case 6: $msg = "Your server is not correctly setup - missing tmp folder. Please ask your server admins for assitance.";
                        break;
                    case 7: $msg = "Failed to save backup to tmp folder. Please try again, or ask your server admins for assistance";
                        break;
                    case 8: $msg = "The backup upload was interrupted. Please try again.";
                        break;
                    default: $msg = "An unknown error occurred. Please try again, and if this continues, please contact support.";
                        break;
                }
                die($msg);
            }
            $fileParts = pathinfo($_FILES['Filedata']['name']);
            $ext = strtolower($fileParts['extension']);
            $destination = $this->dbFile;
            if(in_array($ext, $this->_fileTypes)) {
                $tempFile = $_FILES['Filedata']['tmp_name'];
                if($ext === 'gz') $destination .= '.gz';
                if(!move_uploaded_file($tempFile,$destination)) die('Unable to save uploaded file. Please check that /administrator/components/com_migrateme/'.$destination.' exists and is writable.');
                echo 1;
                die();
        	} else {
        		echo 'Invalid file type. Please upload a .sql, .gz or .txt based database.';
                die();
        	}
        }
        echo 'Unable to upload this file. It may be too large for your system to accept. Please see the User Guide for more information on how to increase your maximum upload size, or how to upload the file with FTP';
        die();
    }

    public function getTable() {
        
        //SKIP SOME TABLES
        if(in_array($this->savetable, $this->ignore_tables)) {
            $this->log("Skipping import of {$this->origtable}. This table is automatically maintained in Joomla 3.");
            return false;
        }
        //SKIP DEPRECATED TABLES
        if(in_array($this->savetable, $this->deprecated_tables)) {
            $this->log("Skipping import of {$this->origtable}. This table is deprecated in Joomla 3.");
            return false;
        } else {
            
            $this->tab = $this->savetable;
            if(isset($this->remap_tables[$this->savetable])) $this->tab = $this->remap_tables[$this->tab];
            $this->table = $this->db->quoteName('#__'.$this->tab);
            
            //CHECK IF THE TABLE EXISTS
            $sql = "SHOW TABLES LIKE '".$this->db->getPrefix().$this->savetable."'";
            $this->db->setQuery($sql);

            //IF TABLE DOESN'T EXIST, CREATE IT
            if(!$res = $this->db->loadResult()) {
                $this->log("Automatically creating the table {$this->origtable}");
                
                //RECREATE KNOWN STRUCTURE
                if(array_key_exists($this->tab,$this->unexistingStructures)) {
                    $this->createUnexistingStructure($this->tab);
                } else {
                    //RECREATE UNKNOWN STRUCTURE
                    
                    //GET THE CREATE TABLE DIALOG
                    $sql = "SHOW CREATE TABLE ".$this->db->quoteName('mm_'.$this->origtable);
                    $this->db->setQuery($sql);
                    $res = $this->db->loadAssoc();
                    $query = str_replace('mm_', '', $res['Create Table']);
                    $query = str_replace('TYPE=', 'ENGINE=', $query);
                    $pos = strpos($query, '`');
                    $prefix = substr($query, ($pos+1));
                    $pos = strpos($prefix, '_');
                    $prefix = substr($prefix,0,$pos);
                    $query = str_replace($prefix, '#_', $query);
                    $this->db->setQuery($query);
                    try {
                        $result = $this->db->query();
                    }
                        catch (Exception $e) {
                        $this->log("WARNING: Unable to create table {$this->table}. Full MySql query: ".$query);
                    }
                    
                    //SPECIAL FOR K2 ITEMS - THEIR INSTALLER DOESN'T UPDATE CORRECTLY:
                    if($this->tab === 'k2_items') {
                        $query = "ALTER TABLE {$this->table} ADD COLUMN `language` CHAR(7) NOT NULL";
                        $this->db->setQuery($query);
                        try {
                            $this->db->query();
                        }
                            catch (Exception $e) {
                            //JUST CONTINUE;
                        }
                    }
                }
            
            //IF TABLE DOES EXIST, DELETE EXISTING DATA WHERE NEEDED
            } else {
                if((int)$this->overwrite !== 2 && (int) $this->resume !== 1 && (int) $this->fresh !== 0) {
                    $sql = "DELETE FROM {$this->table} ";
                    if($this->tab === 'users') {
                        $user = JFactory::getUser();
                        $userid = (int) $user->id;
                        $sql .= "WHERE `id` != '{$userid}'";
                        $query = "DELETE FROM `#__user_usergroup_map` WHERE `user_id` != '{$userid}'";
                        $this->db->setQuery($query);
                        $this->db->query();
                    }
                    if($this->tab === 'categories') $sql .= "WHERE `id` > '1' AND `path` != 'uncategorised'";
                    elseif($this->tab === 'menu') $sql .= "WHERE `id` > '1' AND `client_id` != '1'";
                    elseif($this->tab === 'modules') $sql .= "WHERE `id` > '1' AND `client_id` != '1'";
                    if($this->savetable === 'sections') $sql .= " AND `note` LIKE '%sid%'";
                    $this->db->setQuery($sql);
                    try {
                        $result = $this->db->query();
                    }
                    catch (Exception $e) {
                        $this->log("WARNING: Unable to delete existing content from {$this->table}. Full MySql query: ".$sql);
                    }
                    $sql = "ALTER TABLE  {$this->table} AUTO_INCREMENT = 1";
                    $this->db->setQuery($sql);
                    try {
                        $result = $this->db->query();
                    }
                    catch (Exception $e) {
                        $this->log("WARNING: Unable to update auto increment on {$this->table} table. Full MySql query: ".$sql);
                    }
                }
            }
        }
        return true;
    }
    
    function mapFields($data) {
        
        //SPECIAL EXCEPTION FOR MENU - GET THE RIGHT COMPONENT ID WHERE POSSIBLE
        if($this->tab === 'menu') {
            if(($temp = $this->getComponent($data['link']))!==FALSE) {
                unset($data['componentid']);
                $data['component_id'] = $temp;
            }
        }
        
        //SPECIAL EXCEPTION FOR MODULES - DON'T IMPORT ADMIN MODULES
        if($this->tab === 'modules') {
            if(isset($data['client_id']) && (int) $data['client_id'] === 1) return false;
        }
        
        //SPECIAL EXCEPTION FOR K2 ITEMS - FIX LANGUAGE
        /*
        if($this->tab === 'k2_items') {
            if(isset($data['language'])) {
                $data['language'] = '*';
            }
        }
        */
        
        //GET ORIGINAL CATEGORY AND SECTION IDs
        if(in_array($this->tab, $this->note_tables)) {
            $data['note'] = $this->getNote($data);
        }
        
        //FIX USER TYPES
        if($this->tab === 'users') {
            $data = $this->fixUser($data);
            $this->getUserGroup($data);
        }
        
        //MAKE SURE ALIAS FIELD HAS CONTENT
        if(isset($data['alias']) && strlen(trim($data['alias']))<1) {
            if(isset($data['title'])) $data['alias'] = $data['title'];
            else if(isset($data['name'])) $data['alias'] = $data['name'];
        }

        //ENSURE ALL ITEMS ARE CHECKED OUT
        unset($data['checked_out'], $data['checked_out_time']);

        if(isset($this->map[$this->tab])) {

            $mtemp = $this->map[$this->tab];

            foreach($mtemp as $field=>$replace) { //LOOP THROUGH THE FIELD MAPS
                
                //DISCARD DEPRECATED FIELDS
                if($replace === 0) {
                    unset($data[$field]);
                    continue;
                }
                
                $content = '';
                
                //FORCE A VALUE
                if(substr($replace,0,1) === '!') {
                    $content = substr($replace,1);
                    $data[$field] = $content;
                    continue;
                }

                //USE ANOTHER FIELD'S VALUE
                elseif(substr($replace,0,1) === '$') {
                    if(isset($data[substr($replace,1)])) {
                        $data[$field] = $data[substr($replace,1)];
                    }
                    continue;
                }
                
                //SET VALUE TO NULL
                elseif($replace === 'NULL') {
                    $data[$field] = NULL;
                    continue;
                }
                
                //USE ANOTHER FIELD AND DISCARD THE ORIGINAL FIELD
                elseif(substr($replace,0,1) === '&') {
                    if(isset($data[substr($replace,1)])) {
                        $data[$field] = $data[substr($replace,1)];
                        unset($data[substr($replace,1)]);
                    }
                    continue;
                }
                
                //ADD 1 TO THE CURRENT NUMBER
                elseif(substr($replace,0,1) === '+') {
                    if(!isset($data[$field])) $data[$field] = 0;                    
                    $content = ((int)$data[$field] + 1);
                    $data[$field] = $content;
                    continue;
                }
                
                //USE A FUNCTION TO FIND THE VALUE
                elseif(substr($replace,0,1) === '%') {
                    if(!isset($data[$field])) $data[$field] = '';
                    $func = substr($replace,1);
                    $content = $this->$func($data[$field]);
                    $data[$field] = $content;
                    continue;
                }
            }
        }
        return $data;
    }
    
    function getNote($data) {
        $note = '';
        if($this->savetable === 'categories') {
            if(isset($data['section'])) $note .= 's'.(int)$data['section'];
            $note .= 'c'.(int)$data['id'];
        }
        else if($this->savetable === 'sections') {
            $note .= 'sid'.(int)$data['id'];
        }
        else if($this->savetable === 'menu') {
            $note .= 'm'.(int)$data['id'];
        }
        else if($this->savetable === 'modules') {
            $note = (int)$data['id'];
        }
        return $note;
    }
    
    function getScope($scope) {
        return 'com_'.$scope;
    }
    
    function getCatId($id) {
        $query = 'SELECT '.$this->db->quoteName('id')
            .' FROM '.$this->db->quoteName('#__categories')
            .' WHERE '.$this->db->quoteName('note')
            .' LIKE '.$this->db->quote('%c'.$id)
            .' LIMIT 1';

        $this->db->setQuery($query);
        $res = $this->db->loadResult();        
        if((int)$res > 0) return $res;
        $query = 'SELECT '.$this->db->quoteName('id')
            .' FROM '.$this->db->quoteName('#__categories')
            .' WHERE '.$this->db->quoteName('path')
            .' = '.$this->db->quote('uncategorised')
            .' AND '.$this->db->quoteName('extension')
            .' = '.$this->db->quote('com_content')
            .' LIMIT 1';
        $this->db->setQuery($query);
        $res = $this->db->loadResult();
        if((int)$res > 0) return $res;
        return 2;
    }
    
    function getMenuId($id) {
        $id = (int)$id;
        if($id === 0) return 1;
        $query = 'SELECT '.$this->db->quoteName('id')
            .' FROM '.$this->db->quoteName('#__menu')
            .' WHERE '.$this->db->quoteName('note')
            .' = '.$this->db->quote('m'.$id)
            .' LIMIT 1';
        $this->db->setQuery($query);
        $res = $this->db->loadResult();
        if((int)$res > 0) return $res;
        return 1;
    }
    
    function getComponent($string) {
        $string = strtolower($string);
        $pcre = '#com_[a-z0-9]+#';
        if(preg_match($pcre,$string,$matches)) {
            $com = $matches[0];
            $query = 'SELECT '.$this->db->quoteName('extension_id')
                .' FROM '.$this->db->quoteName('#__extensions')
                .' WHERE '.$this->db->quoteName('type')
                .' = '.$this->db->quote('component')
                .' AND '.$this->db->quoteName('element')
                .' = '.$this->db->quote($com)
                .' ORDER BY '.$this->db->quote('id')
                .' ASC LIMIT 1';
            $this->db->setQuery($query);
            $res = $this->db->loadResult();
            if((int)$res > 0) return $res;
        }
        return false;
    }
    
    function getSection($type) {
        $type = strtolower($type);
        if((int) $type > 0) $type = 'com_content';
        elseif(strlen($type)<1) $type = 'com_content';
        else {
            $query = "SELECT DISTINCT(`extension`) as ext FROM `#__categories`";
            $this->db->setQuery($query);
            $res = $this->db->loadAssocList();
            $ok = 0;
            foreach($res as $r) {
                if(strpos($type, $r['ext']) !== FALSE) {
                    $type = $r['ext'];
                    $ok = 1;
                    break;
                }
            }
        }        
        return $type;
    }
    
    function getLink($string) {
        
        $string = strtolower($string);
        
        //GET THE OPTION
        $option = '';
        $pcre = '#option=com_[a-z]+#';
        if(preg_match($pcre,$string,$options)) {
            $option = str_replace('option=','',$options[0]);
        }
        
        //GET THE VIEW
        $pcre = '#view=[a-z]+#';
        preg_match($pcre,$string,$views);
        if(count($views)>0) {
            $view = str_replace('view=','',$views[0]);
            if($option === 'com_content') {
                $views = array (
                    'frontpage' => 'featured',
                    'categories' => 'category'
                );
                if(array_key_exists($view,$views)) $string = str_replace($view,$views[$view],$string);
                if($view === 'category') {
                    $pcre = '#id=[0-9]+#';
                    preg_match($pcre,$string,$ids);
                    if(count($ids)>0) {
                        $id = str_replace('id=','',$ids[0]);
                        $newid = $this->getCatId($id);
                        $string = str_replace('id='.$id, 'id='.$newid, $string);
                    }
                }
            }
            return $string;
        }
        
        if($option === 'com_weblinks') {
            if(count($views)>0) return $string;
            else {
                $string .= "&view=categories&id=0";
                return $string;
            }
        }
        
        if($option === 'com_newsfeeds') {
            $string = str_replace('&task=view', '', $string);
            $string = str_replace('&feedid=', '&id=',$string);
            $string = str_replace('com_newsfeeds', 'com_newsfeeds&view=newsfeed', $string);
            return $string;           
        }
        return $string;
    }
    
    function getJson($params) {
        $array=explode("\n",$params);
        $newarray = array();
        $continue = 0;
        foreach($array as $x) {
            if($continue > 0) {
                $continue--;
                continue;
            }
            $temp = explode("=",$x);
            if(strlen($temp[0])<2) continue;
            if($temp[0] === 'code_written' || $temp[0] === 'notepad') { //JUMI FIX
                $str = $temp[0];
                $len  = strlen($str.'=');
                $pos0 = strpos($params,$str.'=')+$len;
                if($str === 'code_written') $str2 = 'source_code_storage';
                elseif($str === 'notepad') $str2 = 'code_written';
                $pos1 = strpos($params,$str2.'=') - $pos0;
                $string = substr($params,$pos0,$pos1);
                $temp[1] = $string;
                $continue = (substr_count($string,"\n")-1);
            }
            if(($temp[0] === 'menu_image' || $temp[0] === 'image') && $temp[1] == '-1') $temp[1] = '';
            if($temp[0] === 'show_page_title') $temp[0] = 'show_page_heading';
            if($temp[0] === 'timezone') {
                $temp[1] = '';
                if(!in_array('admin_style',$newarray)) $newarray['admin_style'] = '';
            }
            $newarray[$temp[0]] = (isset($temp[1])) ? $temp[1] : '';
        }
        
        return json_encode($newarray);
    }
    
    function fixModule($module) {
        $array = array('mod_mainmenu' => 'mod_menu', 'mod_latestnews' => 'mod_articles_latest');
        if(key_exists($module, $array)) return $array[$module];
        return $module;
    }
    
    
    function getState($id) {
        if($id == '-1') return 2;
        else return $id;
    }
    
    function createUnexistingStructure($table='') {
        $structure = $this->unexistingStructures[$table];
        $this->db->setQuery($structure);
        try {
            $result = $this->db->query();
        }
        catch (Exception $e) {
            $this->log("WARNING: Unable to recreate table structure. Database error: ".$e->getMessage());
            return;
        }
        $this->log('Automatically recreated structure for '.$this->tab);
        return;
    }
    
    function insertContent($data) { //INSERT CONTENT DIRECTLY ROW BY ROW
        if(isset($data['alias'])) $data['alias']= $this->fixAlias($data['alias']); //FIX DUPLICATE ALIASES
               
        //CHECK FOR DUPLICATES
        if(isset($data['id'])) {
            $query = "SELECT `id` FROM {$this->table} WHERE `id` = '{$data['id']}' LIMIT 1";
            $this->db->setQuery($query);
            $res = $this->db->loadResult();
            if((int)$res > 1) return;
        }

        //BUILD & EXECUTE THE INSERT QUERY
        $sql = "INSERT INTO {$this->table} SET ";
        $n = 0;
        foreach($data as $field=>$val) {
            if($n++ > 0) $sql .= ',';
            $sql .= $this->db->quoteName($field).'=';//
            if($val === NULL) $sql .= ' NULL ';
            else $sql .= $this->db->quote($val);
        }
        $this->db->setQuery($sql);
        try {
            $result = $this->db->query();
        }
        catch (Exception $e) {
            $this->log("WARNING: Unable to save this content. Database error: ".$e->getMessage());
            return;
        }
        
        //ADD MODULE TO MODULE MENU TABLE
        if($this->tab === 'modules') {
            $table = 'mm_'.$this->origtable.'_menu';
            $id = $this->db->insertId();
            $oldid = (int)$data['note'];
            //DELETE ANY OLD MODULE MENU ASSIGNMENT
            $query = "DELETE FROM `#__modules_menu` WHERE `moduleid` = '{$id}'";
            try {
                $this->db->setQuery($query);
                $this->db->query();
            }
            catch (Exception $e) {
                //DO NOTHING
            }
            
            $query = "SELECT `menuid` FROM `{$table}` WHERE `moduleid` = '{$oldid}'";
            try {
                $this->db->setQuery($query);
                $mods = $this->db->loadAssocList();
            }
            catch (Exception $e) {
                 $this->log("Unable to automatically update Module Menu item for module{$id}");
                 return; 
            }
            foreach($mods as $m) {
                $query = "SELECT `id` FROM `#__menu` WHERE `note` = 'm{$m['menuid']}' LIMIT 1";
                try {
                    $this->db->setQuery($query);
                    $temp = $this->db->loadAssoc();
                }
                catch (Exception $e) {
                    //DO NOTHING
                    return;
                }
                $mid = 0;
                if(isset($temp['id']) && $temp['id'] > 0) $mid = $temp['id'];
                $query = "INSERT INTO `#__modules_menu` SET `moduleid` = '{$id}', `menuid` = '{$mid}'";
                try {
                    $this->db->setQuery($query);
                    $result = $this->db->query();
                }
                catch (Exception $e) {
                    //CONTINUE;
                }
            }
        }
    }
    
    function insertAssets($data) {
        
        global $mainframe;
        
        if(isset($data['alias'])) $data['alias'] = $this->fixAlias($data['alias']); //FIX DUPLICATE ALIASES

        /* THIS IS WIERDLY NEEDED FOR ACCURATE PARENT ID */
        if($this->tab === 'categories' || $this->tab === 'sections' || $this->tab === 'menu') {
            $temp = array('alias' => $data['alias'], 'parent_id' => $data['parent_id'], 'extension' => $data['extension']);
            if($this->tab === 'menu') $row = JTable::getInstance('menu');
            else $row = JTable::getInstance('category');
            if (!@$row->bind($temp)) $this->log('Unable to bind PARENT ID: '.$row->getError());
            if(!@$row->store()) $this->log('Unable to store PARENT ID: '.$row->getError());
            $data['id'] = $row->id;
            $data['asset_id'] = $row->asset_id;
        }
        
        /* THIS IS WIERDLY NEEDED FOR ACCURATE CONTENT ID */
        if($this->tab === 'content' && (int)$this->overwrite !== 2) {
            $origid = $data['id'];
            unset($data['id']);
            $query = 'DELETE FROM '.$this->db->quoteName('#__assets').' WHERE '.$this->db->quoteName('name').' = '.$this->db->quote('com_content.article.'.$origid).' LIMIT 1';
            $this->db->setQuery($query);
            try {
                $result = $this->db->query();
            }
            catch (Exception $e) {
                $this->log("WARNING: Unable to delete existing assets. Full MySql query: ".$query);
            }
            $query = 'DELETE FROM '.$this->db->quoteName('#__content').' WHERE '.$this->db->quoteName('id').' = '.$this->db->quote($origid).' LIMIT 1';
            $this->db->setQuery($query);
            try {
                $result = $this->db->query();
            }
            catch (Exception $e) {
                $this->log("WARNING: Unable to delete existing content. Full MySql query: ".$query);
            }
        }

        //GET THE ROW READY
        $row = JTable::getInstance($this->getTableAlias($this->tab));
        $rules = array('core.create' => '', 'core.delete' => '', 'core.edit' => '', 'core.edit.state' => '', 'core.edit.own' => '');
        $row->setRules($rules);
        
        //SAVE THE NEW ASSET
        if (!@$row->bind($data)) $this->log('Unable to bind '.$this->origtable.'. '.$row->getError());
        if(!@$row->store()) {
            $data = implode(',', $data);
            $this->log('Unable to save '.$this->origtable.'. '.$row->getError()."\nDATA: ".$data);
            return;
        }
        $rowid = $row->id;
        $asset_id = $row->asset_id;
        
        //REBUILD TABLE
        if($this->tab === 'categories' || $this->tab === 'menu' || $this->tab === 'sections') {
            $row = JTable::getInstance($this->getTableAlias($this->tab));
            $row->rebuild();
        }
        
        /* THIS IS WIERDLY NEEDED FOR ACCURATE CONTENT ID - PART 2 */
        if($this->tab === 'content') {
            $query = 'UPDATE '.$this->db->quoteName('#__assets').' SET '
                .$this->db->quoteName('name').' = '
                .$this->db->quote('com_content.article.'.$origid)
                .' WHERE '
                .$this->db->quoteName('id').'='
                .$this->db->quote($asset_id);
            $this->db->setQuery($query);
            try {
            $result = $this->db->query();
            }
            catch (Exception $e) {
            $this->log("WARNING: Unable to update asset table. Full MySql query: ".$query);
            }
                
            $query = 'UPDATE '.$this->db->quoteName('#__content').' SET '
            .$this->db->quoteName('id').' = '
            .$this->db->quote($origid)
            .' WHERE '
            .$this->db->quoteName('id').'='
            .$this->db->quote($rowid);
            $this->db->setQuery($query);
            try {
            $result = $this->db->query();
            }
            catch (Exception $e) {
            $this->log("WARNING: Unable to update existing content table. Full MySql query: ".$query);
            }
            $sql = "ALTER TABLE {$this->table} AUTO_INCREMENT =".(int)$origid;
            $this->db->setQuery($sql);
            try {
                $result = $this->db->query();
            }
            catch (Exception $e) {
                $this->log("WARNING: Unable to update auto increment. Full MySql query: ".$sql);
            }
        }
    }
    
    function postMigrationUpdates() {
        //UPDATE MENU PARENTS
        if($this->tab === 'menu') {
                $query = 'SELECT '.$this->db->quoteName('id')
                    .','.$this->db->quoteName('parent_id')
                    .','.$this->db->quoteName('type')
                    .','.$this->db->quoteName('params')
                    .','.$this->db->quoteName('link')
                    .' FROM '.$this->db->quoteName('#__menu')
                    .' WHERE '.$this->db->quoteName('note')
                    .' != '.$this->db->quote(''); 
                $this->db->setQuery($query);
                $res = $this->db->loadAssocList();
                if(count($res)>0) foreach($res as $r) {
                    
                    $params = $r['params'];
                    $link = $r['link'];
                    $parent = $this->getMenuId($r['parent_id']); //FIND THE ORIGINAL PARENT ID
                    
                    if($r['type'] === 'alias') {
                        $linkid = strtolower($link);
                        $linkid = str_replace('index.php?itemid=', '', $linkid);
                        $linkid = $this->getMenuId($linkid);
                        $tempparams = (array)json_decode($params);
                        $tempparams['aliasoptions'] = (string) $linkid;
                        $params = json_encode($tempparams);
                        $link = 'index.php?Itemid=';
                    }
                    
                    $query = "UPDATE {$this->table} SET "
                        .$this->db->quoteName('parent_id')
                        .' = '.$this->db->quote($parent).' , '
                        .$this->db->quoteName('params')
                        .' = '.$this->db->quote($params).' , '
                        .$this->db->quoteName('link')
                        .' = '.$this->db->quote($link)
                        .' WHERE '.$this->db->quoteName('id')
                        .' = '.$this->db->quote($r['id']).' LIMIT 1';
                    $this->db->setQuery($query);
                    try {
                        $result = $this->db->query();
                    }
                    catch (Exception $e) {
                        $this->log("WARNING: Unable to update the menu. Full MySql query: ".$query);
                    }
                }
                //REBUILD THE MENU TABLE
                $row = JTable::getInstance($this->getTableAlias($this->tab));
                $row->rebuild();
        }

        //CUSTOM KUNENA UPDATE
        if($this->tab === 'kunena_users') {
            $userid = (int) $this->getUser();
            $query = 'REPLACE INTO '.$this->table.' SET '.$this->db->quoteName('userid').' = '.$this->db->quote($userid);
            $this->db->setQuery($query);
            try {
                $result = $this->db->query();
            }
            catch (Exception $e) {
                $this->log("WARNING: Unable to add global Kunena user. Full MySql query: ".$query);
            }
        }
    }
    
    function fixMenuType ($type='') {
        foreach(array('url', 'alias', 'separator') as $a) {
            if(strtolower($type) === $a) return $a;
            elseif(strtolower($type === 'menulink')) return 'alias';
            else return 'component';
        }
    }
    
    function fixUser($data) {
        $n = 0;
        $temp = $data['username'];
        do {
            $user = $temp;
            if($n > 0) $user .= '-'.$n;
            $query = 'SELECT '.$this->db->quoteName('id')
                .' FROM '.$this->table
                .' WHERE '.$this->db->quoteName('username')
                .' = '.$this->db->quote($user)
                .' LIMIT 1';
            $this->db->setQuery($query);
            $res = $this->db->loadAssoc();
            $n++;
        } while ((int)$res > 0);
        $data['username'] = $user;
        return $data;
    }
    
    function fixAlias($alias, $id=0) {
        $id = (int)$id;
        $n = 0;
        $temp = strtolower($alias);
        if($this->aliases === 1) {
            $temp = preg_replace("/[^A-Za-z0-9-]/", '-', $temp);
            $temp = preg_replace("/[-]+/", '-', $temp);
        }
        $temp2 = str_replace("-",'',$temp);
        if(strlen($temp2)<1) $temp = date('Y-m-j-H-i-s');
        do {
            $alias = $temp;
            if($n > 0) $alias .= '-'.$n;
            $query = 'SELECT '.$this->db->quoteName('id')
                .' FROM '.$this->table
                .' WHERE '.$this->db->quoteName('alias')
                .' = '.$this->db->quote($alias);
            if($id > 0) $query .= " AND `id` != '{$id}' "; // && (int)$this->overwrite !== 2
            $query .= ' LIMIT 1';
            $this->db->setQuery($query);
            try {
                $res = $this->db->loadAssoc();
            }
            catch (Exception $e) {
                $this->log("Skipping alias check for ".$this->origtable);
            }
            $n++;
        } while (isset($res['id']));
        return $alias;
    }
    
    function getUser() {
        $query = 'SELECT '
            .$this->db->quoteName('id')
            .' FROM '.$this->db->quoteName('#__users')
            .' ORDER BY '.$this->db->quoteName('lastvisitdate')
            .' DESC LIMIT 1';
        $this->db->setQuery($query);
        $res = $this->db->loadResult();
        return (int)$res;
    }
    
    function getUserGroup($data) {
        $map = array(
            'Super Administrator' => 'Super Users',
            'Administrator' => 'Administrator',
            'Manager' => 'Manager',
            'Public Back-end' => 'Guest',
            'Publisher' => 'Publisher',
            'Editor' => 'Editor',
            'Author' => 'Author',
            'Registered' => 'Registered',
            'Public Front-end' => 'Public'
        );
        
        $type = $data['usertype'];
        
        if(array_key_exists($data['usertype'], $map)) {
            $type = $map[$type];
        }
        $query = "SELECT `id` FROM `#__usergroups` WHERE `title` = '{$type}' LIMIT 1";
        $this->db->setQuery($query);
        $group = (int)$this->db->loadResult();
        if($group < 1) {
            $type = 'Public';
            $this->log("WARNING: Unable to find correct Usergroup for user # {$data['id']}. Set as PUBLIC.");
            $query = "SELECT `id` FROM `#__usergroups` WHERE `title` = '{$type}' LIMIT 1";
            $this->db->setQuery($query);
            $group = (int)$this->db->loadResult();
        }
        try {
            $query = "REPLACE INTO `#__user_usergroup_map` SET `user_id` = '{$data['id']}', `group_id` = '{$group}'";
            $this->db->setQuery($query);
            $this->db->query();
        }
        catch (Exception $e) {
            $this->log("WARNING: Unable to SAVE correct Usergroup for user # {$data['id']}");
        }
    }
    
    function getTableAlias($table) {
        $array = array(
            'categories' => 'category',
            'sections' => 'category'
        );
        $table = (isset($array[$table])) ? $array[$table] : $table;
        return $table;
    }

}

