<?php 
/* -------------------------------------------
Component: com_MigrateMe
Author: Barnaby V. Dixon
Email: barnaby@php-web-design.com
Copywrite: Copywrite (C) 2013 Barnaby Dixon. All Rights Reserved.
License: http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
---------------------------------------------*/
defined('_JEXEC') or die('Restricted access');
JToolBarHelper::title(JText::_('MigrateMe: Joomla Pro Migration'), 'migrateme');
JToolBarHelper::custom( 'config', 'help', 'config', JText::_('Configuration'), 0,0 );
?>
<script src="components/com_migrateme/resources/js/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="components/com_migrateme/resources/uploadifive/jquery.uploadifive.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="components/com_migrateme/resources/uploadifive/uploadifive.css" />
<link rel="stylesheet" type="text/css" href="components/com_migrateme/resources/css/stylesheet.css" />
<div id="mm_overcast">&nbsp;</div>
<div id="migrateme">
<form action="index.php?option=com_migrateme" method="post" name="adminForm" id="adminForm">
	<input type="hidden" name="option" value="com_migrateme" />
	<input type="hidden" name="task" value="" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>

<div id="mm_taboo" class="mm_curved">
<div class="mm_top"><img src="components/com_migrateme/resources/images/migrate-me.png" alt="Migrate Me" style="width: 400px;" />
<p style="font-size: 15px;"><em>Professional Joomla! Migration</em></p></div>
<div class="mm_insert">
<h3 style="margin:0 0 14px 0;">Your data has multiple table prefixes</h3>
<p>Your Joomla! 1.5 database has multiple table prefixes. This can cause problems with data conflicts and overwrites.</p>
<p>To avoid this, please carefully select only the tables you want to import. For best results, please do not import data from any other<br />migration components.</p>
<div id="mm_ok">OK!</div>
</div>
</div>

<div class="mm_curved" id="mm_main">
<div class="mm_top">
<a href="components/com_migrateme/migrateme.pdf" target="_blank"><img src="components/com_migrateme/resources/images/question-mark-icon.png" alt="Click for help" style="width: 80px; float: right" /></a>
<img src="components/com_migrateme/resources/images/migrate-me.png" alt="Migrate Me" style="width: 400px;" />
<p style="font-size: 15px;" style="float: left; width: 200px; border: 1px solid red;"><em>Professional Joomla! Migration</em></p>
</div>
<div class="mm_insert">
<div class="mm_left">
<div id="mm_scroll">
<div class="mm_step" id="mm_stepuploaded">
    <h3>Use the uploaded database file?</h3>
    <p>A database file has already been uploaded. Would you like to migrate this?</p>
    <div class="btn_no" onclick="startFresh();">No</div>
    <div class="btn_yes" onclick="unpackFile();">Yes</div>
    <div class="ajax">Please wait...</div>
</div>
<div class="mm_step" id="mm_stepresume">
    <h3>Resume your last migration?</h3>
    <p>An incomplete migration has been detected. Would you like to resume your last migration?</p>
    <div class="btn_no" onclick="startFresh();">No</div>
    <div class="btn_yes" onclick="resumeMigration();">Yes</div>
    <div class="ajax">Please wait...</div>
</div>
<div class="mm_step" id="mm_step1">
    <h3>Step 1: Import your Joomla! 1.5 backup</h3>
    <div id="mm_html5"><p>Sorry! This is extension is built in HTML5.</p><p>You'll need to use a modern browser such as Google Chrome.</p></div>
    <div id="mm_import">
        <input id="file_upload" name="file_upload" type="file" multiple="false" />
    	<div id="queue"></div>
    </div>
    <hr />
</div>
<div class="mm_step" id="mm_step2">
    <h3>Step 2: Select the data to migrate</h3>
    <div class="ajax">Analysing your database</div>
    <div id="mm_step2_content">
        <input type="checkbox" id="selectAll" name="selectAll" value="1" /><label for="selectAll">Select All</label><br />
        <div id="mm_select"></div>       
    </div>
</div>
</div>
<div class="mm_step" id="mm_step3">
    <hr />
    <h3>Step 3: Start the migration</h3>
    <div id="startMigration">Begin migration</div> 
    <div class="ajax"></div>
</div>
<div class="mm_step" id="mm_step4" style="display:none">
    <h3>Migration is complete.</h3>
    <p>Migration is fully complete.</p>
    <p>To view the log file, with full details of all changes, please click here.</p>
        <a target='_blank' href="components/com_migrateme/log.txt"><div id="showLog">Show the Log file</div></a>
</div>
</div>
<div class="mm_right">
<div class="mm_compatibility">
<h3 style="margin: 0;">Compatibility check</h3>
<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin-left: 6px;">
<?php
$checks = array();
$checks[] = array('safe_mode', ' Safe mode OFF', 'Safe mode ON');
$checks[] = array('memory', 'Available memory: ');
foreach($checks as $check) {
    $temp = $check[0];
    if($temp === 'memory') {
        $text = $check[1].$this->checks[$temp].'Mb';
        if($this->checks[$temp] < 64) $status = 0;
        else $status = 1;
    } else {
        $text = ($this->checks[$temp] === 1) ? $check[1] : $check[2];
        $status = $this->checks[$temp];
    } 
    echo "<tr><td class='status-{$status}'>&nbsp;</td><td class='check'>{$text}</td></tr>";
} ?>
<tr>
    <td id="ajax-check-img" class="status-2">&nbsp;</td>
    <td id="ajax-check-text" class="check">AJAX verifying</td>
</tr>
<tr>
    <td class="status-3" width="20">&nbsp;</td>
    <td class="check">Maximum import size: <?php echo $this->checks['upload_limit']; ?>Mb</td>
</tr>
</table>
</div>
<hr style="margin: 10px 0;" />
<h3>Quick Start Instructions</h3>
<p>To use Migrate Me successfully, please follow these instructions:</p>
<ol>
    <li>Download a backup of your original Joomla! 1.5 database
        <ul>
            <li>Make sure you include the <strong>structure</strong> and the <strong>data</strong></li>
            <li>Save the backup in .gz or .sql format</li>
            <li>Optionally install DataSafe Pro on your Joomla 1.5 system to create a fast, reliable backup</li>
        </ul>
    </li>
    <li>Select the backup file using the button in Step 1.</li>
    <li>Select the data you want to migrate</li>
    <li>Click the Begin Migration button</li>
    <li>Once migration is complete, view the log file for any issues.</li>
    <li>Enjoy your new Joomla! system complete with all your Joomla! 1.5 data</li>
</ol>
<p><a href="components/com_migrateme/migrateme.pdf" target="_blank">Click here to read the full user guide.</a></p>
</div>
</div>
</div>
</div>
<script type='text/javascript'>
var tablen = 0;
var complete = 0;
jQuery.noConflict();
jQuery(document).ready(function() {
<?php if($this->resume === 1) {
    echo "showStep('uploaded');\n";
} else if($this->resume === 2) {
    echo "showStep('resume');\n";
} else {
    echo "showStep(1);\n";
} ?>
});

</script>
<script src="components/com_migrateme/resources/js/migrateme.js" type="text/javascript"></script>