<?php 
/* -------------------------------------------
Component: com_MigrateMe
Author: Barnaby V. Dixon
Email: barnaby@php-web-design.com
Copywrite: Copywrite (C) 2013 Barnaby Dixon. All Rights Reserved.
License: http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
---------------------------------------------*/
defined('_JEXEC') or die('Restricted access');
JToolBarHelper::title(JText::_('MigrateMe: Joomla 3.5 Pro Migration'), 'migrateme');

JToolBarHelper::custom( 'save', 'save', 'save', JText::_('Save Config'), 0,0 );
JToolBarHelper::custom( 'cancel', 'cancel', 'cancel', JText::_('Cancel'), 0,0 );
?>
<script src="components/com_migrateme/resources/js/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="components/com_migrateme/resources/uploadifive/jquery.uploadifive.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="components/com_migrateme/resources/uploadifive/uploadifive.css" />
<link rel="stylesheet" type="text/css" href="components/com_migrateme/resources/css/stylesheet.css" />
<div id="mm_overcast">&nbsp;</div>
<div id="migrateme">

<form action="index.php?option=com_migrateme" method="post" name="adminForm" id="adminForm">
	<input type="hidden" name="option" value="com_migrateme" />
	<input type="hidden" name="task" value="" />
	<?php echo JHTML::_( 'form.token' ); ?>

<div class="mm_curved">
<div class="mm_top">
<a href="components/com_migrateme/migrateme.pdf" target="_blank"><img src="components/com_migrateme/resources/images/question-mark-icon.png" alt="Click for help" style="width: 80px; float: right" /></a>
<img src="components/com_migrateme/resources/images/migrate-me.png" alt="Migrate Me" style="width: 400px;" />
<p style="font-size: 15px;" style="float: left; width: 200px; border: 1px solid red;"><em>Professional Joomla! Migration</em></p>
</div>

<div class="mm_insert">

<h1>Configuration</h1>

<fieldset id="mm_config_fieldset">

<div class="mm_field">
<label for="mm_speed">Migration speed: </label>
<select name="mm_speed" id="mm_speed">
<?php
$array = array('0' => 'Crazy fast! (dedicated server)', '1' => 'Quite fast (virtual server)', '2' => 'Normal speed (fast shared host)', '5' => 'Slow (slow shared host)', '30' => 'Extremely slow (GoDaddy/cheap shared host)');
foreach($array as $a=>$b) {
    $sel=($this->speed === (int)$a) ? 'selected="selected"' : '';
    echo "<option value='{$a}' {$sel}>{$b}</option>";
} ?>
</select>
<div class='info showMe'>Set the speed level to suit your server.

If you get strange popup errors, 500 errors, or the migration fails for any reason, please reduce the migration speed and then resume the migration.

The recommended level for most servers is Normal.

If you have a fast or dedicated server, but still get popup errors, you might have Denial of Service protection on your server, which prevents more than a certain number of AJAX requests per minute. If this happens, please reduce the speed here.</div>
</div>

<div class="mm_field">
<label for="mm_overwrite">Overwrite content: </label>
<select name="mm_overwrite" id="mm_overwrite">
<?php
$array = array('1'=>'Yes','2' => 'No');
foreach($array as $a=>$b) {
    $sel=($this->overwrite === (int)$a) ? 'selected="selected"' : '';
    echo "<option value='{$a}' {$sel}>{$b}</option>";
} ?>
</select>
<div class='info showMe'>The recommended setting is 'Yes', which will remove any existing content and allow you to maintain your old Article IDs. However, if you want to keep any old content, and append new migration content, select 'No'. 

WARNING: Please use this setting with care. If you select 'No', you will need to manually delete duplicates. Also, your old article IDs may change, which can break your old links and menu items!</div>
</div>

<div class="mm_field">
<label for="mm_aliases">Standardise aliases: </label>
<select name="mm_aliases" id="mm_aliases">
<?php
$array = array('1'=>'Yes','2' => 'No');
foreach($array as $a=>$b) {
    $sel=($this->aliases === (int)$a) ? 'selected="selected"' : '';
    echo "<option value='{$a}' {$sel}>{$b}</option>";
} ?>
</select>
<div class='info showMe'>The recommended setting is 'Yes', which will ensure all aliases are updated to the latest Joomla standards. However, please note that this will remove any non-ASCII characters, so may affect your SEO. If you want to maintain your old alias structures, select 'No'.</div>
</div>
</fieldset>
</div>

</div>
</form>
</div>
<script src="components/com_migrateme/resources/js/migrateme.js" type="text/javascript"></script>