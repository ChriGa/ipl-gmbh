<?php
/**
 * @package     LOGman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

/**
 * Activity Model Entity
 *
 * @author  Arunas Mazeika <https://github.com/amazeika>
 * @package Joomlatools\Component\LOGman
 */
class ComLogmanModelEntityActivity extends ComActivitiesModelEntityActivity implements KObjectInstantiable
{
    /**
     * Entity map
     *
     * @var $array
     */
    static private $__entities = array();

    public function __construct(KObjectConfig $config)
    {
        parent::__construct($config);

        $plugins = JPluginHelper::getPlugin('logman');

        $package = $this->package;

        foreach ($plugins as $plugin)
        {
            if ($plugin->name == $package) {
                $this->getObject('translator')->load('plg:logman.' . $package);
            }
        }
    }

    /**
     * Instantiate the object
     *
     * @param   KObjectConfigInterface $config      Configuration options
     * @param 	KObjectManagerInterface $manager	A KObjectManagerInterface object
     * @return  KObjectInterface
     */
    public static function getInstance(KObjectConfigInterface $config, KObjectManagerInterface $manager)
    {
        $class = $manager->getClass($config->object_identifier, false);

        if ($class == get_class())
        {
            if ($entity = self::__findEntity($config->data, $manager)) {
                return $manager->getObject($entity, $config->toArray());
            }
        }

        return new $class($config);
    }

    protected function _initialize(KObjectConfig $config)
    {
        $config->append(array(
            'object_table'  => $config->data->package,
            'object_column' => 'id'
        ));

        parent::_initialize($config);
    }

    /**
     * Activity image getter.
     *
     * The image is a CSS class pointing to an image representing the activity.
     *
     * @return string The activity image class name.
     */
    public function getPropertyImage()
    {
        $images = array(
            'publish'   => 'icon-ok-circle',
            'unpublish' => 'icon-remove-circle',
            'trash'     => 'icon-trash',
            'add'       => 'icon-plus-sign',
            'edit'      => 'icon-edit',
            'delete'    => 'icon-remove',
            'archive'   => 'icon-inbox'
        );

        // Default.
        $image = 'icon-task';

        if (in_array($this->verb, array_keys($images))) {
            $image = $images[$this->verb];
        }

        return $image;
    }

    /**
     * Activity scripts getter.
     *
     * @return string|null Scripts to be included with the rendered HTML activity, null if no scripts.
     */
    public function getPropertyScripts()
    {
        return null;
    }

    protected function _objectConfig(KObjectConfig $config)
    {
        $config->append(array(
            'url' => 'option=com_' . $this->package . '&task=' . $this->name . '.edit&id=' . $this->row
        ));

        parent::_objectConfig($config);
    }

    protected function _generatorConfig(KObjectConfig $config)
    {
        parent::_generatorConfig($config->append(array('objectName' => 'LOGman')));
    }

    protected function _providerConfig(KObjectConfig $config)
    {
        parent::_providerConfig($config->append(array('objectName' => 'LOGman')));
    }

    /**
     * Extension translations loader.
     *
     * @param string $extension The name of the extension to load translations for.
     * @return boolean True if loaded, false otherwise.
     */
    protected function _loadTranslations($extension)
    {
        $lang = JFactory::getLanguage();

        $parts = explode('_', $extension);
        $type  = $parts[0];

        $result = false;

        switch ($type)
        {
            case 'com':
                $result = ($lang->load($extension . '.sys', JPATH_BASE, null, false, false)
                           || $lang->load($extension . '.sys', JPATH_ADMINISTRATOR . '/components/' . $extension,
                        null, false, false)
                           || $lang->load($extension . '.sys', JPATH_BASE, $lang->getDefault(), false, false)
                           || $lang->load($extension . '.sys', JPATH_ADMINISTRATOR . '/components/' . $extension,
                        $lang->getDefault(), false, false));
                break;
            case 'mod':
                $metadata = $this->getMetadata();

                if ($metadata) {
                    $clients = array($metadata->client);
                } else {
                    $clients = array('admin', 'site'); // Search on both.
                }

                foreach ($clients as  $client)
                {
                    $path = $client == 'admin' ? JPATH_ADMINISTRATOR : JPATH_SITE;

                    $result = ($lang->load($extension . '.sys', $path, null, false, false)
                               || $lang->load($extension . '.sys', $path . '/modules/' . $extension,
                            null, false, false)
                               || $lang->load($extension . '.sys', $path, $lang->getDefault(), false, false)
                               || $lang->load($extension . '.sys', $path . '/modules/' . $extension,
                            $lang->getDefault(), false, false));

                    if ($result) break;
                }

                break;
            case 'plg':
                $result = ($lang->load($extension . '.sys', JPATH_BASE, null, false, false)
                    || ((count($parts) == 3) && $lang->load($extension . '.sys',
                            JPATH_PLUGINS . '/' . $parts[1] . '/' . $parts[2]))
                    || $lang->load($extension . '.sys', JPATH_BASE, $lang->getDefault(), false, false));
                break;
        }

        return $result;
    }

    /**
     * Looks up for an activity entity object given its data.
     *
     * @return mixed A string or identifier object, false if not found.
     */
    private static function __findEntity($data, KObjectManagerInterface $manager)
    {
        if (!isset(self::$__entities[$data->package])) {
            self::$__entities[$data->package] = array();
        }

        if (!isset(self::$__entities[$data->package][$data->name]))
        {
            self::$__entities[$data->package][$data->name] = false;
            $identifiers                                   = array(
                sprintf('plg:logman.%s.activity.%s', $data->package, $data->name),
                sprintf('plg:logman.%1$s.activity.%1$s', $data->package));

            foreach ($identifiers as $identifier)
            {
                if ($manager->getClass($identifier, false))
                {
                    self::$__entities[$data->package][$data->name] = $identifier;
                    break;
                }
            }
        }

        return self::$__entities[$data->package][$data->name];
    }

    /**
     * Overridden for using custom router route object.
     */
    protected function _getRoute($url)
    {
        if (!is_string($url)) throw new InvalidArgumentException('The URL must be a query string');

        return $this->getObject('com://admin/logman.activity.router.route', array('url' => array('query' => $url)));
    }
}
