<?php
/**
 * @package     LOGman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

/**
 * Extension Model Entity
 *
 * @author  Arunas Mazeika <https://github.com/amazeika>
 * @package Joomlatools\Component\LOGman
 */
class ComLogmanModelEntityExtension extends ComExtmanModelEntityExtension
{
	public function save()
	{
		$result = parent::save();

		if ($result)
		{
			$db = JFactory::getDBO();
            $db->setQuery("SELECT id FROM #__modules WHERE module = 'mod_logman' AND published <> -2 AND position = '' AND client_id = 1");
            $id = $db->loadResult();
            if ($id)
            {
                $db->setQuery(sprintf("UPDATE `#__modules` SET position = 'cpanel', ordering = -1, published = 1, params = '{\"limit\":\"10\",\"direction\":\"desc\"}'
				    	WHERE id = %d LIMIT 1", $id));
                $db->query();
                $db->setQuery("REPLACE INTO #__modules_menu VALUES ($id, 0)");
                $db->query();
            }

            if ($id)
            {
                $table	= JTable::getInstance('menu');
                $table->bind(array('id' => $id));
                $table->delete();
            }
		}

		return $result;
	}
}