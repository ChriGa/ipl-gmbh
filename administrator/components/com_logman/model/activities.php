<?php
/**
 * @package     LOGman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

/**
 * Activities Model
 *
 * @author  Arunas Mazeika <https://github.com/amazeika>
 * @package Joomlatools\Component\LOGman
 */
class ComLogmanModelActivities extends ComActivitiesModelActivities {

    public function __construct(KObjectConfig $config)
    {
        parent::__construct($config);

        $state = $this->getState();

        $state->insert('usergroup', 'int');
    }

    protected function _buildQueryJoins(KDatabaseQueryInterface $query)
    {
        parent::_buildQueryJoins($query);

        $state = $this->getState();

        if ($state->usergroup)
        {
            $query->join('user_usergroup_map AS users_groups', 'tbl.created_by = users_groups.user_id', 'INNER');
        }
    }

    protected function _buildQueryWhere(KDatabaseQueryInterface $query)
    {
        parent::_buildQueryWhere($query);

        $state = $this->getState();

        if ($usergroup = $state->usergroup) {
            $query->where('users_groups.group_id IN :usergroup')->bind(array('usergroup' => $usergroup));
        }
    }
}