<?php
/**
 * @package     LOGman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

/**
 * LOGman Plugin Interface
 *
 * @author  Arunas Mazeika <https://github.com/amazeika>
 * @package Joomlatools\Component\LOGman
 */
interface ComLogmanPluginInterface
{
    /**
     * Adds/logs an activity row.
     *
     * @param array $data The activity data.
     * @return object The activity row.
     */
    public function log($data = array());

    /**
     * Get the a parameter
     *
     * @param  string $name    The name of the setting parameter.
     * @param null    $default The default value.
     * @return mixed The parameter or default value.
     */
    public function getParameter($name, $default = null);
}