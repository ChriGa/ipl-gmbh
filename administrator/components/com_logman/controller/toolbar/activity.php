<?php
/**
 * @package     LOGman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

/**
 * Activity Controller Toolbar
 *
 * @author  Arunas Mazeika <https://github.com/amazeika>
 * @package Joomlatools\Component\LOGman
 */
class ComLogmanControllerToolbarActivity extends ComKoowaControllerToolbarActionbar
{
    protected function _commandPurge(KControllerToolbarCommandInterface $command)
    {
        $command->attribs->href = JRoute::_('index.php?option=com_logman&view=activities&layout=purge&tmpl=koowa', false);
        $command->attribs->append(array('data-koowa-modal' => array('mainClass' => 'logman-purge-modal')));

        $this->_commandDialog($command);
    }

    protected function _commandExport(KControllerToolbarCommand $command)
    {
        $url = 'option=com_logman&view=activities&layout=export&tmpl=koowa';

        if (version_compare(JVERSION, '3.0', '>=')) {
            $command->icon = 'icon-download';
        }

        $command->attribs->href = $this->getController()->getView()->getRoute($url, false, false);
        $command->attribs->append(array('data-koowa-modal' => array('mainClass' => 'logman-export-modal')));

        $this->_commandDialog($command);
    }

    protected function _afterBrowse(KControllerContextInterface $context)
    {
        parent::_afterBrowse($context);

        $controller = $this->getController();

        if ($controller->canPurge()) {
            $this->addPurge();
        }

        $this->addExport();

        if ($controller->canAdmin())
        {
            $enabled = $controller->pluginEnabled();
            $command = $enabled ? 'disable' : 'enable';
            $this->addCommand($command, array(
                'attribs' => array(
                    'data-novalidate' => 'novalidate',
                    'data-action' => 'editPlugin'
                )
            ));
        }

        if ($controller->canAdmin()) {
            $this->addOptions();
        }
    }
}