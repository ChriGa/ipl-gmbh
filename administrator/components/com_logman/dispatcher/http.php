<?php
/**
 * @package     LOGman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

/**
 * Http Dispatcher
 *
 * @author  Arunas Mazeika <https://github.com/amazeika>
 * @package Joomlatools\Component\LOGman
 */
class ComLogmanDispatcherHttp extends ComKoowaDispatcherHttp
{
	protected function _initialize(KObjectConfig $config)
	{
		$config->append(array(
			'controller' => 'activity'
		));

		parent::_initialize($config);
	}
}