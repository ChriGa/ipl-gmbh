<?php
/**
 * @package     LOGman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

/**
 * Behavior Tempplate Helper
 *
 * @author  Arunas Mazeika <https://github.com/amazeika>
 * @package Joomlatools\Component\LOGman
 */
class ComLogmanTemplateHelperBehavior extends ComKoowaTemplateHelperBehavior
{
    /**
     * Loads LOGman's JS library.
     *
     * @param array $config An optional configuration array.
     * @return string HTML containing library loading calls.
     */
    public function logman($config = array())
    {
        $html = '';

        if (!isset(self::$_loaded['logman-js']))
        {
            $html .= '<ktml:script src="media://com_logman/js/logman.js" />';
            self::$_loaded['logman-js'] = true;
        }

        return $html;
    }
}