<?php
/**
 * @package     LOGman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

/**
 * DOCman LOGman plugin.
 *
 * Wires DOCman loggers to Files and DOCman components controllers.
 *
 * Also provides event handlers for DOCman 1.x.
 *
 * @author  Arunas Mazeika <https://github.com/amazeika>
 * @package Joomlatools\Plugin\LOGman
 */
class PlgLogmanDocman extends ComLogmanPluginKoowa
{
    protected function _initialize(KObjectConfig $config)
    {
        $config->append(array(
            'controllers' => array(
                'com://site/docman.controller.download'  => 'plg:logman.docman.logger.download',
                'com://site/docman.controller.document'  => 'plg:logman.docman.logger.document',
                'com://site/docman.controller.submit'    => 'plg:logman.docman.logger.document',
                'com://admin/docman.controller.document' => 'plg:logman.docman.logger.document',
                'com://admin/docman.controller.category' => 'plg:logman.docman.logger.category',
                'com:files.controller.file'              => 'plg:logman.docman.logger.file',
                'com:files.controller.folder'            => 'plg:logman.docman.logger.folder'
            )
        ));

        parent::_initialize($config);
    }
}