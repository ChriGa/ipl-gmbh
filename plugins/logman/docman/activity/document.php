<?php
/**
 * @package     LOGman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

/**
 * Document/DOCman Activity Entity
 *
 * @author  Arunas Mazeika <https://github.com/amazeika>
 * @package Joomlatools\Plugin\LOGman
 */
class PlgLogmanDocmanActivityDocument extends ComLogmanModelEntityActivity
{
    protected function _initialize(KObjectConfig $config)
    {
        $config->append(array(
            'format'        => '{actor} {action} {object.subtype} {object.type} title {object}',
            'object_table'  => 'docman_documents',
            'object_column' => 'docman_document_id'
        ));

        parent::_initialize($config);
    }

    public function getPropertyImage()
    {
        if ($this->verb == 'download') {
            $image = 'icon-download';
        } else {
            $image = parent::getPropertyImage();
        }

        return $image;
    }

    protected function _objectConfig(KObjectConfig $config)
    {
        $config->append(array(
            'subtype' => array('objectName' => 'DOCman', 'object' => true),
            'url'     => 'option=com_docman&view=document&id=' . $this->row
        ));

        parent::_objectConfig($config);
    }
}