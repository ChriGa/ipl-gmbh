<?php
/**
 * @package     LOGman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

/**
 * File FILEman Logger
 *
 * @author  Arunas Mazeika <https://github.com/amazeika>
 * @package Joomlatools\Plugin\LOGman
 */
class PlgLogmanFilemanLoggerFile extends PlgLogmanFilemanLoggerNode
{
    public function getActivityData(KModelEntityInterface $object, KObjectIdentifierInterface $subject)
    {
        $data = parent::getActivityData($object, $subject);

        $metadata = array();

        if ($size = $object->size) {
            $metadata = array('size' => $size);
        }

        if ($object->isImage())
        {
            $metadata['image']  = true;
            $metadata['width']  = $object->width;
            $metadata['height'] = $object->height;
        }
        else $metadata['image'] = false;

        $data['metadata'] = array_merge($data['metadata'], $metadata);

        return $data;
    }

    public function getActivitySubject(KCommandInterface $context)
    {
        $identifier = $context->getSubject()->getIdentifier()->toArray();

        $container = explode('-', $context->result->container);
        $container = $container[0];

        if ($container) {
            $identifier['package'] = $container;
        }

        return $this->getIdentifier($identifier);
    }
}