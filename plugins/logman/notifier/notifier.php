<?php
/**
 * @package     LOGman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

/**
 * Activities Notifier LOGman plugin.
 *
 * @author  Arunas Mazeika <https://github.com/amazeika>
 * @package Joomlatools\Plugin\LOGman
 */

class PlgLogmanNotifier extends ComLogmanPluginNotifier
{
    protected function _initialize(KObjectConfig $config)
    {
        if ($recipients = $config->params->get('recipients', '')) {
            $recipients = explode(',', $recipients);
        }
        else $recipients = array();

        $config->append(array(
            'notifiers' => array(
                'plg:logman.notifier.email' => array(
                    'html'                   => $config->params->get('html', true),
                    'bcc'                    => $recipients,
                    'usergroups'             => $config->params->get('usergroups', array()),
                    'notify_packages'        => $config->params->get('notify_packages', array()),
                    'notify_userlog_actions' => $config->params->get('notify_userlog_actions', array())
                )
            )
        ));

        parent::_initialize($config);
    }
}