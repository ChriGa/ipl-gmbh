<?

/**
 *
 *
 *
 *
 *
 * Javascrip camera.min.js von http://www.pixedelic.com/plugins/camera/
 *
 **/

defined ('_JEXEC' ) or die ( 'Restricted access' );

$count = count($images);
($count > 0) ? $slideclass = "camera_wrap" : $slideclass = "no_img";
?>
<?//JQuery Call für Ausführing der JS Slideshow?>
	<script>
		jQuery(function(){			
			jQuery('.camera_wrap').camera({
				playPause: true,
				playPause: false,
				navigation: false,
				pagination: false,
				hover: false,
				loader: 'bar',
				height: '<?print $moduleHeight?>',
				fx: '<? print $effect?>',
				time: <? print $interval?>,
				loader: 'none',
				onLoaded: function(){
					<? if(!$autoplay || $count <= 1 ) {?>
					jQuery('.camera_wrap').cameraPause();
					<? }?>
				}
			});
		});
	</script>

<?//Slideshow Inhaltsbereich?>

<div id="cg_slider <?=$moduleID; ?>" class=" <?=$modulClassSuffix; ?>" style="max-width: <?=$moduleWidth; ?>; margin:<?=$marginTopBottom; ?> <?=$marginLeftRight; ?>;">
	<div class="<?=$slideclass?>" >
		<?
			if ( $count > 0 ) { 
				foreach ($images as $image ) {								
						print '<div class="cg_slider_img" style="height: 410px;" data-src="'.$image["imgsrc"].'">';
						if ($image["caption"]) {
							print '<div class="camera_caption fadeIn" style="background-color: rgba('.$rgba.',0.5);" >';
							if ($image["link"]) print '<a href="'.$image["link"].'">';
								print '<'.$modulCapHeader.'>' .$image["caption"].'</'.$modulCapHeader.'>';
							if ($image["link"]) print '</a>';
							print '</div>';
						}
						print '</div>';
					
				}	
			}
			else {
				print '<div class="'.$slideclass.'" style="width: 960px; height: 500px; background: rgba(137, 189, 237, 0.3);">
							<h2 style="position: absolute; margin: 10%;">
								Es wurde kein Bild ausgew&auml;hlt
							</h2>
						</div>';					
			}				
		?>
	</div>
</div>
<div style="clear: both;"></div>

