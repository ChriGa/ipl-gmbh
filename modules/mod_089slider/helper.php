<?
/*
*
*
+
**/



	class modcgslider {
		
		var $num_images = 4;
		
		
		public function getImages ( $params )
		{		
			$images_data = array();
			// preprint($params);
			for ($i=1; $i<= $this->num_images; $i++) {
				$imgsrc = $params->get("slider_img_".$i);
				$cap = $params->get("slider_cap_".$i);
				$link = $params->get("slider_link_".$i);
				if (empty($imgsrc)) continue;
				
				$images_data[] = array("imgsrc" => $imgsrc, "caption" => $cap, "link" => $link );
			}
			// preprint($images_data);
			
			return $images_data;
		}


	    public function includeHead($params){
			$document	= JFactory::getDocument();
			$header = $document->getHeadData();
			$loadJquery = $params->get('load_jquery', '');
			$mainframe = JFactory::getApplication();
			$template = $mainframe->getTemplate();
			
			if($loadJquery === ''){
				$loadJquery =  true;
				foreach($header['scripts'] as $scriptName => $scriptData)
				{
					  if(substr_count($scriptName,'/jquery'))
					  {
							$loadJquery = false;
					  }
				}
			}	
		//Javascripts und Modul-CSS hinzufügen
			if($loadJquery)
			{
				$document->addScript(JURI::root().'modules/mod_089slider/js/jquery.min.js');
			}

			if(file_exists(JPATH_BASE.'/templates/'.$template.'/html/mod_089slider/css/camera_styles.css'))
			{
				$document->addStyleSheet(  JURI::root().'templates/'.$template.'/html/mod_089slider/css/camera_styles.css');
			}
			else{
				$document->addStyleSheet(JURI::root().'modules/mod_089slider/css/camera_styles.css');
			}
			$document->addScript(JURI::root().'modules/mod_089slider/js/jquery.easing.1.3.js');
			$document->addScript(JURI::root().'modules/mod_089slider/js/jquery.mobile.customized.min.js');
			$document->addScript(JURI::root().'modules/mod_089slider/js/camera.min.js');
			//$document->addScript(JURI::root().'modules/mod_cgslider/tmpl/js/default.js');
	            
		}
		
		
		
	/**
	 * Funktion hexadezimal-Farbwert die einen kommagetrennten RGB-Farbcode String zurück gibt 
	 * Author: Chris Gabler
	 * April 2015
	 **/
		public function hex2Rgb ($hex, $params) {
				$hex = $params->get('caption_slider_bckg');
				list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");				
				return "$r, $g, $b";
		}
		
		
		
	}

?>









