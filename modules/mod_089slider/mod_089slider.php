<?

/*
*
*
*
*
*
**/

//Kein direkter Zugriff
defined( '_JEXEC') or die ('Restricetd access');

require_once dirname(__FILE__) . '/helper.php';


$images = array();
$helper = new modcgslider();
$images = $helper->getImages($params);

$moduleID = $module->id;
$moduleWidth = $params->get('module_width', 'auto');
$moduleHeight = $params->get('module_height', '50%');
$marginTopBottom = $params->get('module_marginTopBottom');
$marginLeftRight = $params->get('module_marginLeftRight');
$modulClassSuffix = $params->get('moduleclass_sfx');

$modulCapHeader = $params->get('caption_slider_header');
$modulCapBckg = $params->get('caption_slider_bckg');
$autoplay = $params->get('autoplay', 1);
$interval = $params->get('interval', 3000);
$effect = $params->get('effect', array('sliderLeft'));
if(is_array($effect)){
	$effect = implode(',',$effect);
}
$rgba = $helper->hex2Rgb($modulCapBckg, $params);


$helper->includeHead( $params );


require(JModuleHelper::getLayoutPath('mod_089slider', 'default') );

?>